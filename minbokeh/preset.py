import colorlover as cl
import re

from bokeh.colors import RGB as rgb
from bokeh.models import HoverTool


from pyutils.structures.strs import Strs, KStrs
from pyutils.structures.maps import AttrMap



def coerce_colors(scale, to_color=tuple, to_value=int):
	result = [to_color(map(to_value, re.findall(r'\d+', color))) for color in scale]
	return result

class DefsMinbokeh:
	palette = coerce_colors(cl.scales['8']['qual']['Set2'], to_color=lambda c: rgb(*c))
	paletteb = coerce_colors(cl.scales['8']['qual']['Dark2'], to_color=lambda c: rgb(*c))
	ohlc_keys = KStrs('open high low close adjClose volume')
	date_keys = KStrs('date datetime t')
	
	preset_groups = dict(
		price		= Strs('price open high low close adjClose'),
		perf		= Strs('perf'),
		volume		= Strs('volume'),  )
	
	preset_tools = AttrMap(
		price		= Strs('pan wheel_zoom box_zoom reset save'),
		volume		= Strs('pan'),
		perf		= Strs('pan'),
		indicator	= Strs('pan wheel_zoom'),  )
	
	default_preset = 'indicator'
	_base_cfg = dict(x_axis_type='datetime', plot_width=1000, active_scroll='wheel_zoom')
	preset_cfgs = AttrMap(
		empty		= dict(),
		volume		= dict(_base_cfg, tools=preset_tools.volume, plot_height=100, active_scroll=None),
		perf		= dict(_base_cfg, tools=preset_tools.perf, plot_height=50, active_scroll=None),
		indicator	= dict(_base_cfg, tools=preset_tools.indicator, plot_height=100),
		price		= dict(_base_cfg,
			tools=preset_tools.price, plot_height=500, x_axis_location='above', toolbar_location='left'),  )

fab_hovertool = lambda glyphs, tooltips=None, side='left', **ka: (
	HoverTool(
		renderers=glyphs, tooltips=tooltips or [], attachment=side,
		formatters={'@t': 'datetime'}, mode='vline', line_policy='interp', **ka))

fab_main_tooltip = lambda keys: (
	[   ('t',		'@t{%y%m%d-%H%M}'),
		('ohlc',	'@open{0.0}  @high{0.0}  @low{0.0}  @close{0.0}'),
		('volume',	'@volume{0.000a}'),
	] + sorted([(str(key), f'@{key}{{0.000 a}}') for key in keys]))
gen_hovertool, gen_main_tooltip = fab_hovertool, fab_main_tooltip