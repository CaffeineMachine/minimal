# import numpy as np
# import pandas as pd

from datetime import datetime, timedelta
from pprint import pprint as pp, pformat as pfmt

from bokeh.plotting import Figure
from bokeh.io import output_notebook, show
from bokeh.layouts import column
from bokeh.models import ColumnDataSource, Range, Range1d

from pyutils.defs.typedefs import *
from pyutils.extend.datastructures import *
from pyutils.utilities.iterutils import *
from pyutils.output.colorize import *
from pyutils.utilities.srcinfoutils import *
from pyutils.structures.units import *

from minimal import minnumpy as np
from minimal import minpandas as pd

from .preset import *


Def = DefsMinbokeh
DataFrame = pd.DataFrame
VMINBOKEH = [0, 0, 5]
_verbose, logger = 2, lambda msg, *pa, **ka: print(cyan(msg), *pa, **ka)
log1, log2, log3, *_ = *([logger] * _verbose), *[None]*4
logplot_fmt = 'func: {:16} id: {:8} glyph: {:14} {}'
logplot = lambda funcname, glyph, data: log2(logplot_fmt.format(funcname, glyph.id, nameof(glyph.glyph), [*data.keys()]))
log3 and log3('.'.join(VMINBOKEH))


@dataclass
class PathXY:
	x: float = 0.
	y: float = 0.
	x_seq: list = field(default_factory=list)
	y_seq: list = field(default_factory=list)
	
	def __iter__(self):
		yield from (self.x_seq, self.y_seq)
	def reset(self, x=0., y=0.):
		self.x, self.y = x, y
		self.x_seq.clear()
		self.y_seq.clear()
	def move_to(self, x=0., y=0., update=True):
		self.x, self.y = x, y
		update and self.x_seq.append(self.x)
		update and self.y_seq.append(self.y)
	def move_by(self, x=0., y=0., update=True):
		self.x += x
		self.y += y
		update and self.x_seq.append(self.x)
		update and self.y_seq.append(self.y)
	def __iadd__(self, xy):
		self.move_by(*xy, update=True)
		return self
		
	# properties
	end = property(None, lambda self, xy: self.move_to(*xy, update=True))

def gen_deltas(t_seq, y_seq, scalar_seq, dt, dy):
	''' for each scalar at point (t, y) add 2 connected line segments; 1 vertical and 1 scaled diagonal '''
	dy_fy = dy if callable(dy) else None
	dt_zero = dt*0
	
	pathxy = PathXY()
	for t, y, scalar in zip(t_seq, y_seq, scalar_seq):
		dy = dy_fy and dy_fy(y) or dy
		pathxy.end	= t, y
		pathxy		+= dt_zero, (scalar > 0. and dy or -dy)
		pathxy		+= dt*-abs(scalar), dy*-scalar
		pathxy.end	= nan, nan
	return pathxy

def get_bound(seq, lo=None, hi=None):
	''' get upper and lower in single pass of iterable '''
	for value in seq:
		lo = value if lo is None or value < lo else lo
		hi = value if hi is None or value > hi else hi
	return lo, hi



def is_jupyter_runtime():
	import os
	from pyutils.extend.datastructures import XDict
	
	result = XDict.like(os.environ, 'JUPYTER_PATH|JPY_PARENT_PID')  # check for 2 known jupyter env vars
	return bool(result)

def detect_deltatime(t_seq, window_size=10):
	dt = None
	t_prev = datetime.min
	for t in t_seq[:window_size]:
		t = t.to_pydatetime()
		dt = min(t - t_prev, dt or timedelta.max)
		t_prev = t  # todo: raise if not strictly increasing
	log3 and log3(f'detect_deltatime() => {dt}')
	return dt

def configure_child(*figs):
	for fig in figs:
		fig.toolbar.logo = None
		fig.toolbar_location = None
		fig.xaxis.visible = False
	
def configure_linked_pan(parent_fig, *child_figs):
	parent_x_range = parent_fig.x_range
	for child_fig in child_figs:
		child_fig.x_range = parent_x_range
	configure_child(*child_figs)
	
def apply_theme():
	from bokeh.io import curdoc
	from bokeh.themes import Theme
	from glob import glob
	import re
	theme_file_fmt = '/home/zeroone/workspacehost/checkout/online/bokeh/bokeh/themes/{}.json'
	theme_presets = glob(theme_file_fmt.format('*'))
	pp(theme_presets)
	curdoc().theme = Theme(filename=theme_presets[1])

def fab_figure(preset=None, **kws):
	''' fabricate a chart figure configured with given preset key from: indicator, price, volume, empty '''
	log3 and log3(f'Making new figure using preset \'{preset}\'')
	preset = preset in Def.preset_cfgs and preset or Def.default_preset
	preset_cfg = Def.preset_cfgs[preset]
	preset_cfg.update(kws)
	
	fig = Figure(**preset_cfg)
	fig.grid.grid_line_alpha=0.3
	return fig


class FigureMap(FillMap):
	def __init__(self, subfig_map):
		super().__init__(fill=lambda key: fab_figure(preset=key), on_miss='fab')
		# super().__init__(None)
		self.key_groups = self.subfig_map = subfig_map
		self.series_subfig_map = {}
		self.group_tooltips = {}
		self.preload()
		self.price_tooltip = self.fab_price_plot_tooltip()
		
	def preload(self):
		if is_jupyter_runtime():
			output_notebook()  # call to bokeh output method needed before creating figures to avoid memory leaks
		
		items = [*{**Def.preset_groups, **self.key_groups}.items()]
		for subfig, datasets in items:
			if not datasets: continue
			datasets = datasets + (isntint(subfig) and [subfig] or [])  # add non-index subfig name as a dataset
			links = XDict.set_links(self, datasets, self[subfig])
			self.series_subfig_map.update(links)
			
	def fab_price_plot_tooltip(self):
		fig_key_map = XDict.get_links_map(self.series_subfig_map)
		price_plot_keys = fig_key_map[self['price']]  # all keys of series plotted on price figure
		non_price_plot_keys = set(self) - set(price_plot_keys+['datetime', 'volume', 't'])
		tooltips = fab_main_tooltip(non_price_plot_keys)
		return tooltips
		
	def show(self):
		fig_subsets = XDict.subsets(self, ['price', 'volume'])
		figs = []
		
		for fig_subset in fig_subsets:
			for fig in fig_subset.values():
				assert isinstance(fig, Figure)
				if not fig in figs:
					figs.append(fig)

		if len(figs) == 1:
			show(*figs)
		elif len(figs) > 1:
			configure_linked_pan(*figs)
			show(column(*figs))
		
	def details(self):
		links_map = FillMap(missing=lambda key: Strs())
		for key, fig in self.items():
			links_map[fig._id].append(key)
		log3 and log3(f'FigureMap(links_map:\n{pfmt(links_map)})')
		log3 and log3(f'  summary: {len(self)} keys mapped to {len(links_map)} figures')




features_pos		= TStrs('t_pos  vnet_pos  held_pos  perf_asset_pos')
features_dpos		= TStrs('t_dpos  y_dpos  dpos  load_dpos  pload_dpos  pos0_dpos  pos1_dpos')  # net_dpos: pos = sum of dpos; net_dpos comports with ptrn
# features_segments	= TStrs('tl_seg  tr_seg  nc_seg  ncs_seg  yl_seg  yr_seg  yar_seg  ybr_seg  ycr_seg  state_seg')
features_segments	= TStrs('tl  tr  nc  ncs  yl  yr  yar  ybr  ycr  state', to_str='{}_seg'.format)
features_zone		= TStrs('t_zone  p4_zone  accent_zone')
features_delta		= TStrs('t_delta  y_delta  z_delta')

ptrn_feature_key = '^({data}_)?({feature})_({dset})'
ptrn_pos		= re.compile(fr'^({"|".join(features_pos)})_([\w\d]+)')
ptrn_dpos		= re.compile(fr'^({"|".join(features_dpos)})_([\w\d]+)')
ptrn_segments	= re.compile(fr'^({"|".join(features_segments)})_([\w\d]+)')
ptrn_zone		= re.compile(fr'^({"|".join(features_zone)})_([\w\d]+)')
ptrn_delta		= re.compile(fr'^({"|".join(features_delta)})_([\w\d]+)')


def fet_dset_of_key(df, key, features=None):
	features = features or Def.known_features
	
	dset, dset_id, feature_id, plotter = [], None, None, None
	ptrns_fet_key = [ptrn_feature_key.format(feature=fet_id, data='.*', dset='.*') for fet_id in features]
	finds = {(re.findall(ptrn, key)+[None])[0] for ptrn in ptrns_fet_key} - {None}
	if finds:
		found, *more = finds
		assert not more, 'Ambiguity from multiple valid features signatures: ' + ' '.join(finds)
		*_, feature_id, dset_id = found
		dset = fet_dset_of_ids(df, feature_id, dset_id)
		plotter = features[feature_id]
	return dset, dset_id, feature_id, plotter
def fet_dset_of_ids(df, feature_id, dset_id):
	ptrn = ptrn_feature_key.format(data='.*', feature=feature_id, dset=dset_id)
	dset = lre(ptrn, df.keys())
	return dset
def fet_validate_dset(features, dset_ids, error=True, cond=set.issubset):
	valid = cond(set(features), dset_ids)
	assert not error or valid, f'not {getattr(cond, "__name__", cond)}({features}, {dset_ids})'
	return valid



dark = lambda color: color.darken(.12)
light = lambda color: color.lighten(.12)
def default_color_sets(key):
	intervals = [15, 30, 1]
	ind, *_ = [ind for ind, interval in enumerate(intervals) if re.search(fr'{interval}n?$', key)]+[1]
	color = Def.palette[ind]
	color = (ind == 2 and color) or (key[0] == 'guard'[0] and dark(color)) or (key[0] == 'usher'[0] and light(color)) or color
	return color
	
def plot_data(*dfs:DataFrame, price=None, volume=None, perf=None, subfigs=(), dt=None, show=True, col_sets=None, **ka):
	col_sets = col_sets or default_color_sets
	
	# initialize figures
	subfigs = subfigs if isinstance(subfigs, dict) else {ind:subfig for ind, subfig in enumerate(subfigs or ())}
	subfigs.update({k:v for k, v in dict(price=price, volume=volume, perf=perf).items() if v is not None})
	figs = FigureMap(subfigs)
	avoid_keys = set()  # addt. filter for cases such as keys with no depiction or without dedicated plot

	for df in dfs:
		data_keys = set(df.keys())
		line_keys = data_keys - Def.ohlc_keys - Def.date_keys
		line_keys = sorted(([*map(int, re.findall(r'\d+', line_key)), line_key] for line_key in line_keys))
		dt = dt or detect_deltatime(df.t)
		if 'open' in df and 'close' in df:
			df['delta'] = df.close - df.open
			df['status_colors'] = np.select([df.delta >= 0.], ['green'], 'red')
		
		# construct figures for ohlc bars and volume if contained in dataframes
		if Def.ohlc_keys & data_keys:
			plot_band(df, dt=dt, fig=figs['price'], tooltips=figs.price_tooltip)
			if 'volume' in data_keys:
				plot_volume(df, dt=dt, fig=figs['volume'])
	
		# construct a new line for each dataseries and add to figure for corresponding line group
		for (*rate, line_key), color in zip(line_keys, Def.palette[0:]):
			if line_key in avoid_keys: continue
			if line_key not in figs:
				log1 and print(f'Missing figure for {line_key!r}')
				continue
			
			log2 and log2('\n'+line_key)
			dset, dset_id, fet_id, plotter = fet_dset_of_key(df, line_key)
			if plotter:
				avoid_keys.update(dset)
				if fet_id not in ('zone',):
					plotter(df, data_id=dset_id, fig=figs[line_key], color=col_sets(dset_id))
			else:
				plot_line(df, y=line_key, fig=figs[line_key], line_color=color)
			
	# show all figures if needed
	show and figs.show()
	
	return figs


def plot_line(data, y=None, x='t', fig=None, **ka):
	fig = fig or fab_figure()
	
	# apply glyphs for given data sets
	line = fig.line(x=x, y=y, line_width=2, alpha=0.7, legend_label=y+' ', source=data, **ka)
	logplot and logplot(ffuncname(), line, data)
	
	# add formatted tooltip to glyphs
	if not len(fig.hover):
		hovertool = fab_hovertool([line])
		fig.add_tools(hovertool)
	fig.hover[0].tooltips.append((y, f'@{y}{{0.2f}}'))
	
	return fig

def plot_band(data, tooltips=None, fig=None, **ka):
	fig = fig or fab_figure()
	# outer = np.hstack((data.high, data.low[::-1]))
	lo, hi = [bound * scale for bound, scale in zip(get_bound(data.close[::20]), [.45, 1.5])]
	source = ColumnDataSource(dict(t=data.t, open=data.open, close=data.close, low=data.low, high=data.high, volume=data.volume))  # inner=inner,
	
	# apply glyphs for given data sets
	areas = [
		fig.line(x='t', y='close', legend_label='close', line_color='blue', line_width=1, source=source),
		fig.square(x='t', y='close', legend_label='close', line_color='blue', size=4, source=source),
		fig.varea(y1='low', y2='high', x='t', color='blue', alpha=0.08, legend_label='high/low', source=source),  ]
	fig.y_range.update(start=lo, end=hi)
	
	# add formatted tooltip to glyphs
	hovertool = fab_hovertool(areas[:1], tooltips, 'right')
	fig.add_tools(hovertool)
	
	return fig

def plot_zones(data, data_id, fig=None, **ka):
	# coerce inputs
	fig = fig or fab_figure(preset='price', **ka)
	ka = {**dict(fill_color=ka.pop('color', 'orange'), alpha=0.2), **ka}
	
	# prepare plot data source
	keys = k_x, k_p4, k_accent = [*map(f'{{}}_{data_id}'.format, features_zone)]
	data = imapitems(keys, data, on_miss='continue', fab=dict)
	source = ColumnDataSource(data)
	
	# apply glyphs for given data sets
	areas = {
		k_p4 in data and fig.patch(x=k_x, y=k_p4, line_width=0, source=source, **ka),
		k_accent in data and fig.patch(x=k_x, y=k_accent, line_color='magenta', line_width=1, source=source, **ka),  }
	[logplot and logplot(ffuncname(depth=2), glyph, data) for glyph in areas if glyph]

	# # add formatted tooltip to glyphs
	# hovertool = fab_hovertool(volume_bars, {'volume': '@volume{0.2f}'})
	# fig.add_tools(hovertool)
	
	return fig

def plot_deltas(data, data_id, unit_perc = 2., fig=None, **ka):
	# coerce inputs
	fig = fig or fab_figure(preset='price', **ka)
	ka = {**dict(line_color='orange', line_width=4), **ka}
	
	# prepare plot data source
	keys = k_x, k_y, k_z = [*map(f'{{}}_{data_id}'.format, features_delta)]
	data = lmap(keys, data, on_miss='continue')
	# x_seq, y_seq = gen_deltas(*data, dt=TM.second*30, dy=lambda y: y*.02)
	x_seq, y_seq = gen_deltas(*data, dt=timedelta(seconds=60), dy=lambda y: y*.006)
	source = ColumnDataSource({k_x:x_seq, k_y:y_seq})
	
	# apply glyphs for given data sets
	deltas = fig.line(x=k_x, y=k_y, source=source, **ka)

	# # add formatted tooltip to glyphs
	# hovertool = fab_hovertool(volume_bars, {'volume': '@volume{0.2f}'})
	# fig.add_tools(hovertool)
	
	return fig

def plot_segments(data, data_id, unit_perc = 2., fig=None, **ka):
	# coerce inputs
	fig = fig or fab_figure(preset='price', **ka)
	ka = {**dict(color='orange'), **ka}
	
	# prepare plot data source
	keys = k_t0, k_t1, k_y0, k_y1, k_ya, k_yb, k_yc, k_state = [*map(f'{{}}_{data_id}'.format, features_segments)]
	data = imapitems(keys, data, on_miss='continue', fab=dict)
	color_of_state = Strs('gray yellow lightgreen lightyellow white')
	base_color = ka.pop('color')
	dt = timedelta(seconds=int(re.search(r'(\d*)$', data_id).group(1) or 0)*8/15)
	source, ystates_prev, state_ind = FillAttrMap(list), None, 0
	for t0, t1, y0, y1, ya, yb, yc, state_ind_next in zip(*iseqmap(data, keys)):
		ystates = [ya, yb, yc, yb, ya]
		ystate0, ystate1 = (ystates_prev or ystates)[state_ind], ystates[state_ind]
		
		source.t00 += [t0+dt, t0+dt, nan]
		source.t10 += [t1+dt, t0+dt, nan]
		source.yab += [ya, yb, nan]
		source.ybc += [yb, yc, nan]
		[source[f'ystate{ind}'].extend(state_ind == ind and [ystate0, ystate1, nan] or [nan]*3) for ind in range(5)]
		source.colors += [color_of_state[state_ind]]*3
		
		state_ind, ystates_prev = state_ind_next, ystates
	ka['source'] = ColumnDataSource(source)
	
	# apply glyphs for given data sets
	lines = [
		fig.line(x='t00', y='yab', line_width=2, color=base_color, line_dash='dotted', **ka),
		fig.line(x='t00', y='ybc', line_width=2, color=base_color, **ka),
		fig.line(x='t10', y='ystate0', line_width=2, color='black', **ka),
		fig.line(x='t10', y='ystate1', line_width=4, color='darkgoldenrod', **ka),
		fig.line(x='t10', y='ystate2', line_width=8, color='darkgreen', **ka),
		fig.line(x='t10', y='ystate3', line_width=4, color='darkcyan', **ka),
		fig.line(x='t10', y='ystate4', line_width=2, color='gray', **ka),
	]
	[logplot and logplot(ffuncname(depth=2), glyph, data) for glyph in lines]

	# # add formatted tooltip to glyphs
	# hovertool = fab_hovertool(volume_bars, {'volume': '@volume{0.2f}'})
	# fig.add_tools(hovertool)
	
	return fig

def plot_segments(data, data_id, unit_perc = 2., fig=None, **ka):
	# coerce inputs
	fig = fig or fab_figure(preset='price', **ka)
	ka = {**dict(color='orange'), **ka}
	
	# prepare plot data source
	keys = k_tl, k_tr, *_, k_yar, k_ybr, k_ycr, k_state = [*map(f'{{}}_{data_id}'.format, features_segments)]
	data = imapitems(keys, data, on_miss='continue', fab=dict)
	width_of_state, alpha_of_state = [3, 4, 7, 6, 5], [.4, .55, .7, .85, 1.]
	dt = timedelta(seconds=int(re.search(r'(\d*)$', data_id).group(1) or 0)*8/15)
	
	source, tl, ncl, yal, ybl, ycl, state_ind = FillAttrMap(list), data[k_tr][0], 0., data[k_yar][0], data[k_ybr][0], data[k_ycr][0], 0
	for _, tr, nc, ncs, yl, yr, yar, ybr, ycr, state_ind_next in zip(*iseqmap(data, keys)):
		yoffset = ybr > yar and 50. or -50
		ycl_ofs = yar + ycl-yal
		source.t += [tr+dt, None, None]
		source.ts += [[tr+dt, tr+dt], [tr+dt, tr+dt], [tl+dt, tr+dt]]
		source.ys += [[yar, ybr], [ybr, ycr], [ycl, ycl_ofs]]
		source.alphas += [.3, ncl != nc and .8 or 0., alpha_of_state[state_ind]]
		source.widths += [2, 2, width_of_state[state_ind]]
		source.dashes += ['solid', 'solid', 2 >= state_ind and 'solid' or 'dotted']
		source.ychain += [ycr+yoffset, nan, nan]
		source.ychains += [not ncs and ycr or nan, nan, nan]
		source.text += [f'{ncs-nc}\'\n{nc}', None, None]
		# source.text += [f'{not nc and str(ncs) or str()}\n{nc}', None, None]
		
		# setup next iteration
		tl, ncl, yal, ybl, ycl, state_ind = tr, nc, yar, ybr, ycr, state_ind_next
	ka['source'] = ColumnDataSource(source)
	
	# apply glyphs for given data sets
	lines = [
		fig.multi_line(xs='ts', ys='ys', alpha='alphas', line_width='widths', **ka),
		# fig.cross(x='t', y='ychain', size=20, line_width=1, **{**ka, 'line_color':'black'}),
		# fig.x(x='t', y='ychains', size=20, line_width=1, **{**ka, 'line_color':'black'}),
		fig.text(x='t', y='ychain', text='text', text_color=ka['color'], text_align='center', text_baseline='middle', source=ka['source']),
	]
	[logplot and logplot(ffuncname(depth=2), glyph, data) for glyph in lines]

	# # add formatted tooltip to glyphs
	# hovertool = fab_hovertool(volume_bars, {'volume': '@volume{0.2f}'})
	# fig.add_tools(hovertool)
	
	return fig

def plot_dpos(data, data_id, unit_perc = 2., fig=None, **ka):
	# coerce inputs
	fig = fig or fab_figure(preset='price', **ka)
	ka = {**dict(color='orange'), **ka}
	dt_of_len = lambda length: timedelta(seconds=abs(length)*140+2)
	
	# prepare plot data source
	keys = k_x, k_y, k_dpos, k_pload, k_load, k_pos0, k_pos1 = [*map(f'{{}}_{data_id}'.format, features_dpos)]
	data = imapitems(keys, data, on_miss='continue', fab=dict)
	source = FillAttrMap(list)
	for dpos, load, pload, pos0, pos1 in zip(*iseqmap(data, keys[-5:])):
		angle, pos0, pos1 = (dpos > 0. and (45,pos0,pos1)) or (dpos < 0. and (-45,pos0-1.,abs(pos1-1.))) or (0,0.,0.)
		source.angle.append(angle)
		[source[k].append(dt_of_len(v)) for k, v in zip(keys[-5:], (dpos, load, pload, pos0, pos1))]
	source = ColumnDataSource({**data, **source})
	
	# apply glyphs for given data sets
	rays = [
		fig.ray(x=k_x, y=k_y, length=k_dpos, line_width=2, angle='angle', angle_units='deg', source=source, **ka),
		fig.ray(x=k_x, y=k_y, length=k_pos1, line_width=8, angle='angle', angle_units='deg', source=source, **ka),
		fig.ray(x=k_x, y=k_y, length=k_pos0, line_width=2, angle='angle', angle_units='deg', source=source, **{**ka, **{'color':'black'}}),  ]
	[logplot and logplot(ffuncname(depth=2), glyph, data) for glyph in rays]

	# # add formatted tooltip to glyphs
	# hovertool = fab_hovertool(volume_bars, {'volume': '@volume{0.2f}'})
	# fig.add_tools(hovertool)
	
	return fig

def plot_pos(data, data_id, tooltips=None, fig=None, **ka):
	# coerce inputs
	fig = fig or fab_figure()
	
	# prepare plot data source
	keys = k_x, k_net, k_asset, k_perf = [*map(f'{{}}_{data_id}'.format, features_pos)]
	data = dict(data, zeroes=[-1.]*len(data[k_x]))
	# bot, upper = min(data[k_net])*.95, max(data[k_net])*1.05
	lo, hi = [bound * scale for bound, scale in zip(get_bound(data[k_net]), [.95, 1.05])]
	source = ColumnDataSource(data)
	
	# apply glyphs for given data sets
	line = fig.line(x=k_x, y=k_net, color='blue', line_width=3, source=source)
	fig.quad(data[k_x][0], data[k_x][-1], data[k_net][0], [2000, 0], color=Strs('green red'), alpha=.2)
	fig.y_range.update(start=lo, end=hi, bounds=(lo, hi))
	[logplot and logplot(ffuncname(depth=2), glyph, data) for glyph in [line]]
	
	# # add formatted tooltip to glyphs
	hovertool = fab_hovertool([line], {k_net: f'@{k_net}{{0.2f}}'}, side='right', anchor='top_center')
	fig.add_tools(hovertool)
	
	return fig

def plot_candles(data, dt=None, tooltips=None, fig=None):
	fig = fig or fab_figure(preset='price')
	dt = dt or detect_deltatime(data.t)
	w = dt.total_seconds() * 1000

	# apply glyphs for given data sets
	candle_bars = [
		fig.vbar(x='datetime', width=w, top='open', bottom='close', fill_color='status_colors', line_color=None, source=data),  ]
	
	# add formatted tooltip to glyphs
	hovertool = fab_hovertool(candle_bars, tooltips, 'right')
	fig.add_tools(hovertool)

	return fig

def plot_bar_lines(data, dt=None, tooltips=None, fig=None, **ka):
	fig = fig or fab_figure(preset='price')
	dt = dt or detect_deltatime(data.datetime)
	w = dt.total_seconds() * 1000
	
	# apply glyphs for given data sets
	lines = [
		fig.line(x='datetime', y='close', alpha=0., line_width=0, source=data),  # used exclusively as anchor for hovertool
		fig.vbar(x='datetime', width=w*.3, top='high', bottom='low', fill_color='status_colors', line_color=None, source=data),
		fig.ray(x='datetime', y='open', length=w*.5, angle=180, angle_units='deg', color='status_colors', line_width=2, source=data),
		fig.ray(x='datetime', y='close', length=w*.5, angle=0, angle_units='deg', color='status_colors', line_width=2, source=data),  ]
	
	# add formatted tooltip to glyphs
	hovertool = fab_hovertool(lines[:1], tooltips, 'right')
	fig.add_tools(hovertool)

	return fig

def plot_volume(data, dt=None, fig=None, **ka):
	fig = fig or fab_figure(preset='volume', **ka)
	dt = dt or detect_deltatime(data.t)
	w = dt.total_seconds() * 1000
	
	# apply glyphs for given data sets
	volume_bars = [
		fig.vbar(x='t', width=w, top='volume', bottom=0, fill_color='status_colors', line_color=None, source=data),  ]
	fig.y_range.bounds='auto'
	
	# add formatted tooltip to glyphs
	hovertool = fab_hovertool(volume_bars, {'volume': '@volume{0.2f}'}, side='right', anchor='top_center')
	fig.add_tools(hovertool)

	return fig

Def.known_features = dict(
	pos=plot_pos,
	dpos=plot_dpos,
	seg=plot_segments,
	zone=plot_zones,
)