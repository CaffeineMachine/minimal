import os
import re
import time
import matplotlib.pyplot as plt

from pyutils.common.log import *
from pyutils.extend.datastructures import XDict
from threading import Timer
from matplotlib.dates import DayLocator, HourLocator, DateFormatter, drange


class Plot(object):
	_inst = None

	@staticmethod
	def get():
		Plot._inst = Plot._inst or Plot()
		return Plot._inst

	def insert(this, t, groups={}, **kwargs):
		self = this or Plot.get()
		self.t = t
		self.groups = groups
		self.series = kwargs

	def extend(self): pass
	def reset(self): pass

	@staticmethod_self_or_none
	def plot(this, *args, **kwargs):

		self = this or Plot.get()
		self.insert(*args, **kwargs)
		self.show()

	@staticmethod_self_or_none
	def show(this, should_block=False):
		self = this or Plot.get()
		if should_block:
			self._show()
		else:
			timer = Timer(0, self._show)
			timer.start()

	@staticmethod_self_or_none
	def _show(self):

		# slice = 1 # int(AudioConfig.sample_rate/sample_rate)
		# t_sub = t[::slice]
		# freq_sub = freq[::slice]
		# if volume is not None:
		# 	volume_sub = volume[::slice]

		# identify and create axes based on series groupings
		series_axis_map = {}
		for name in self.series:
			# todo: continue if name is already in groups arg
			found = re.findall(r'(.+)\d+$', name)
			series_axis_map[name] = found and found[0] or name

		# plot series mapped to named axes
		unique_axis_names = set(series_axis_map.values())
		fig, axes = plt.subplots(nrows=len(unique_axis_names), sharex=True)
		if axes.__class__.__name__ == 'AxesSubplot':
			axes = (axes,)

		named_axes = dict(zip(unique_axis_names, axes))
		for name, axis in named_axes.items():
			axis.set_title(name)
			if isinstance(self.t[0], datetime):
				axis.xaxis.set_major_locator(DayLocator())
				axis.xaxis.set_minor_locator(HourLocator(range(0, 25, 6)))
				axis.xaxis.set_major_formatter(DateFormatter('%Y-%m-%d'))
				axis.grid(True)

		for series, axis_name in series_axis_map.items():
			named_axes[axis_name].plot(self.t, self.series[series])

		plt.show()



if __name__ == '__main__':
	from gibberish import *
	from pprint import pprint as pp


	# tested and working
	chart = RandomChart(1000, 1)
	chart.plot()

''' 
todo: 
	employ array slice to limit maximum displayed points on figure
	allow specifying axis groups
	configure series display attributes such as color and line pattern
	support extending Plot state and redisplaying
	support clearing Plot state
tested:
	support multiple subplots
	unittests or module execution tests
	plots multiple named data series
	display plot asynchronously
	static method calls to singleton; required due to lack of matplotlib multi threaded support
	all functions except: extend, reset
	enforce single threaded access to Plot._show()
'''
