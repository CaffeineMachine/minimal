from sortedcontainers import SortedSet
from pyutils.defs.typedefs import *


class SortedKeyMap(SortedSet):
	''' a map interface on a sorted set of hashable items '''
	def __getitem__(self, key, inclusive=(True,False)):
		if typeof(key, slice):
			items = self.irange_key(key.start, key.stop, inclusive)
		else:
			items = next(self.irange_key(key, key))
		return items
	def get(self, key, default=None, inclusive=(True,False)):
		if typeof(key, slice):
			items = self.irange_key(key.start, key.stop, inclusive)
		else:
			items = next(self.irange_key(key, key), default)
		return items
	def ind_item(self, ind, default=None, inclusive=(True,False)):
		if typeof(ind, slice):
			item = self.islice(ind.start, ind.stop)
		else:
			item = next(self.islice(ind, ind+1), default)
		return item
	def key_ind(self, key, default=None, inclusive=(True,False)):
		if typeof(key, slice):
			ind = self.irange(key.start, key.stop, inclusive)
		else:
			ind = next(self.irange(key, key), default)
		return ind
	def keys(self, key=None, **ka):
		items = self if key is None else self.__getitem__(key, **ka)
		return (item.key for item in items)
	def items(self):
		return ((item.key, item) for item in self)
	# member aliases
	values = SortedSet.__iter__