# Minimal

A collection of wrappers for prominent python modules.  These wrappers have
been consolidated into the minimal module for ease of access and future
expansion.

## Creators

**Stanley Hailey**

- <http://caffeine-machine.com/>

## License
Released for free under the MIT license, which means you can use it for almost
any purpose (including commercial projects). Credit is appreciate but not required.