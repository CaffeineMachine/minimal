'''
# Jupyter User Interface Components

## Naming Convention:
- Fab: On attribute access uses given attribute name to perform some operation. Behaves as an AttrMapping.
'''

from ipywidgets.widgets import Layout, Output, HTML, Label, Box, HBox, VBox, ToggleButton, Button, GridBox, Text

from pyutils.patterns.poly import *
from pyutils.structures.maps import *
from pymatics.output.streams import OutStreams  # usually neccesary


# class XYFlow_aabc_azzz:
# 	parent_layout = Layout(
# 		flex_flow='row wrap',
# 		align_content='flex-end',
# 		width='400px',  )
#
# 	@classmethod
# 	def fab(cls, height_parent='auto', height_1n='auto', width_1n='auto'):
# 		result = Box(buttons, layout=
# 		return result

class XYFlow_zzza_aabc:
	parent_layout = dict(
		flex_flow='column wrap-reverse',
		align_content='flex-end',
		height='ARG',
	)
	child_0_layout = dict(flex='0 1 100%')
	child_1n_layout = dict(width='ARG', height='ARG')
	
	@classmethod
	def fab(cls, uis, height_parent, width_1n='100px', height_1n='auto'):
		parent_layout = dict(cls.parent_layout, height=height_parent)
		child_1n_layout = dict(width=width_1n, flex=f'0 1 {height_1n}')
		
		for ind, child in enumerate(uis):
			child_layout = ind == 0 and cls.child_0_layout or child_1n_layout
			for key, value in child_layout.items():
				child.layout.set_trait(key, value)
		result = Box(uis, layout=Layout(**parent_layout))
		return result


class Ascii(Output):
	def __init__(self, text, *args, **kargs):
		super().__init__(*args, **kargs)
		self.append_stdout('---')
		self.text = text
	def set_text(self, text):
		self.outputs = (dict(self.outputs[0], text=str(text)),)
	text = property(Name.outputs[0]['text'], set_text)
	



class Actor:
	_text_map_default = FullMap(asis)
	_tooltip_map_default = FullMap(None)
	
	def get_text_map(self):
		result = getattr(self, 'text_map', Actor._text_map_default)
		return result
	def get_tooltip_map(self):
		result = getattr(self, 'tooltip_map', Actor._tooltip_map_default)
		return result
		
		
	

class Interface:
	__slots__ = ['actions']
	
	# properties
	action = property(lambda self: self.actions[0])  # get the primary (first) of given actions
	
	def __init__(self, *actions):
		self.actions = actions
	def interact(self, actor):
		result = Interact(actor, *self.actions)
		return result
	# method aliases
	__call__ = Poly.interact

class UInterface(Interface):
	''' Optional intermediate definition preceding UInterface '''
	_obj_ui_fab = None
	_btn_ui_fab = None
	_layout = None
	
	def __init__(self, *actions, obj_ui_fab=None, btn_ui_fab=None, layout=None):
		'''	:param actions:
			:param box_ui_fab:
			:param obj_ui_fab:
			:param btn_ui_fab:
		'''
		super().__init__(*actions)
		self.obj_ui_fab = obj_ui_fab or self._obj_ui_fab
		self.btn_ui_fab = btn_ui_fab or self._btn_ui_fab
		self.layout = layout or self._layout
	def interact(self, actor):
		result = UInteract(actor, *self.actions,
			actor_ui_fab=self.obj_ui_fab,
			btn_ui_fab=self.btn_ui_fab,
			layout=self.layout,
		)
		return result
	
class Interact(Interface):
	__slots__ = ['_actor']
	
	def __init__(self, actor, *actions):
		super().__init__(*actions)
		self._actor = actor
	def __str__(self):
		return str(self.actor)
	def get_method(self, action=None):
		''' get method for given action name '''
		return getattr(self._actor, action or self.action)
	def invoke(self, action, *args, **kargs):
		''' invoke an action with actor using any given args '''
		method = self.get_method(action)
		result = method(*args, **kargs)
		return result
	def act(self, *args, **kargs):
		''' invoke the primary action with actor using any given args '''
		return self.invoke(self.action, *args, **kargs)
	
	# properties
	actor = property(lambda self: self._actor)
	method = property(get_method)  # get the member function of the primary action
	
	# method aliases
	# __call__ = Poly.act
	__call__ = act

# xy_Ayz_aaa = dict(display='flex', )
xy_Ayz_aaa = dict(display='grid', grid_auto_flow='column', grid_template_columns='1fr', grid_auto_columns='auto')

class UInteract(Interact, Box):
	''' Usage:
	
	'''
	# static members
	_actor_ui_fab	= Fabricator(Ascii, 'text')
	_btn_ui_fab		= Button
	_layout			= dict(xy_Ayz_aaa, grid_gap='8px')
	
	def __init__(self, actor, *actions, actor_ui_fab=None, btn_ui_fab=None, layout=None):
		'''	ctor
			:param actor:
			:param actions:
			:param actor_ui_fab:
			:param btn_ui_fab:
			:param layout:
		'''
		Interact.__init__(self, actor, *actions)
		self._actor_ui_fab = actor_ui_fab or self.__class__._actor_ui_fab
		self._btn_ui_fab = btn_ui_fab or self.__class__._btn_ui_fab
		
		# construct uis
		self.btn_uis = [self._btn_ui_fab(description=action, tooltip='') for action in self.actions]
		self.actor_ui = self._actor_ui_fab(actor)
		self.actor = actor
		Box.__init__(self, [self.actor_ui] + self.btn_uis, layout=layout or self._layout)

		# setup handlers
		for button in self.btn_uis:
			button.on_click(self.on_event)
	def on_event(self, button):
		action = button.description
		print(action)
		# self.actor_ui.text = str(self.interact)  # todo: find right place to make this conversion happen
		self.invoke(action)  # todo: add setting for prioritizing invoke on obj or obj_ui
	def set_actor(self, actor):
		''' apply a new actor and reflect details of actions onto btn_uis as text description and/or tooltip '''
		self._actor = actor
		self._actor_ui_fab.set(self.actor_ui, actor)
		action_text_map = Actor.get_text_map(actor)
		action_tooltip_map = Actor.get_tooltip_map(actor)
		for action, btn_ui in zip(self.actions, self.btn_uis):
			action_text = action_text_map[action]
			action_tooltip = action_tooltip_map[action]
			
			btn_ui.description = action_text
			btn_ui.tooltip = action_tooltip or ''
		
	# properties
	actor = property(lambda self: self._actor, set_actor)

IfaceFab	= InterfaceFab	= AttrFab(Interface)
UIfaceFab	= UInterfaceFab	= AttrFab(UInterface)



# if __name__=='__main__':
# 	# display = asis
# 	obj = {}
# 	objs = [{}, {}, {}]
#
# 	func = Poly.clear
# 	op = func(obj)
# 	op()
#
# 	ClearFab = InterfaceFab.clear
# 	btn = ClearFab(obj)
# 	display(btn)
#
# 	TraceUIFab = UInterfaceFab.setbreak
# 	btn = TraceUIFab(obj)
# 	display(btn)
