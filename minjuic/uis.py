# import itertools as it; chain_iter = it.chain.from_iterable
import inspect

from traitlets import List
from colour import Color
from dataclasses import dataclass, field
from jinja2 import Template
from ipywidgets.widgets import (
	Widget, DOMWidget, Box, HBox, VBox, GridBox, Layout, Output,
	HTML, Label, Button, ToggleButton, Text, IntText, FloatText, Textarea,
	Checkbox, DatePicker, )

from pyutils import typeof, ftee
from pyutils.patterns.props import *
from pyutils.patterns.poly import *
from pyutils.structures.models import *
from pyutils.structures.selections import *
from pyutils.utilities.srcinfoutils import *
from pyutils.structures.strs import *
from pyutils.utilities.iterutils import *




class DefsJuis:
	layout_enum = horz, vert = Strs('horz vert')
	
	xy_Ayz_aaa = dict(display='grid', grid_auto_flow='column', grid_template_columns='80px', grid_auto_columns='auto')

	wide = dict(width='auto')
	tall = dict(height='auto')
	
	class Flex:
		flex = dict(display='flex')
		xy_ab_aa = dict(**flex, flex_flow='row')
		xy_ba_aa = dict(**flex, flex_flow='row-reverse')
		xy_aa_ab = dict(**flex, flex_flow='column')
		xy_aa_ba = dict(**flex, flex_flow='column-reverse')

	class Grid:
		grid = dict(display='grid')
		xy_ab_aa = dict(**grid, grid_auto_flow='column')
		xy_aa_ab = dict(**grid, grid_auto_flow='row')
		xy_aB_ab = dict(**grid, grid_template_columns='80px auto', grid_auto_rows='40px')
		xy_aB_ac = dict(**grid,
			grid_template_columns='80px auto',
			grid_template_rows='50px',
			grid_auto_rows= '40px',
		)
		xy_Awxyz_aaaaa = dict(**grid, grid_template_columns='3fr 1fr 1fr 1fr 1fr')
	
	flexbox, gridbox = Flex.flex, Grid.grid
	
	value_setter = lambda ui, value: setattr(ui, 'value', value)
Defs = DefsJuis


def custom_fmt(self, obj, metadata=None):
	output = {
		'output_type': 'display_data',
		'metadata': metadata or {},
		'data': {
			'text/plain': repr(obj),
			'application/vnd.jupyter.widget-view+json': {
				'version_major': 2,
				'version_minor': 0,
				'model_id': obj._model_id,
			}
		},
	}
	return output

class PresetBox(Box):
	_layout = {}
	_layout_preset = {}
	_ui_fab = None
	_dom_classes = ()
	
	def __init__(self, items=(), *pa, layout=None, ui_fab=None, ui_ka=None, _dom_classes=(), **ka):
		layout = {**self._layout_preset, **self._layout, **(layout or {})}
		_dom_classes = lskip(self._dom_classes, _dom_classes.__contains__) + list(_dom_classes)
		ui_fab and setattr(self, '_ui_fab', ui_fab)
		self.ui_ka = ui_ka or {}
		# items = [self._ui_fab(item, **ui_ka or {}) for item in items] if self._ui_fab else items
		items = [self.ui_fab_conf(item) for item in items] if self._ui_fab else items
		
		super().__init__(items, *pa, layout=layout, _dom_classes=_dom_classes, **ka)
	def ui_fab_conf(self, *pa, **ka):
		return self._ui_fab(*pa, **self.ui_ka, **ka)
	def apply(self, items, inds=None, setter=None):
		# todo: default setter to _ui_fab when available
		setter = setter or Defs.value_setter
		
		uis = []
		for item, ui in zip(items, chain(self.children, igen(self.ui_fab_conf))):
			setter(ui, item)
			uis.append(ui)
			
		apply_partial = False  # todo: based on inds
		if not apply_partial and len(self.children) != len(uis):
			self.children = tuple(uis)
		
class GridRows(PresetBox):
	_layout_preset = {**DefsJuis.Grid.xy_aa_ab}
class GridCols(PresetBox):
	_layout_preset = {**DefsJuis.Grid.xy_ab_aa}

	

# class GridRows(GridBox):
#
# 	def __init__(self, children=None, *args, **kargs):
# 		# children = list(chain_iter(children or []))
# 		children = list(ijoin(children or []))
# 		super().__init__(children, *args, **kargs)


class Ascii(Output):
	def __init__(self, text, *args, **kargs):
		super().__init__(*args, **kargs)
		self.append_stdout('---')
		self.text = text
	def set_text(self, text):
		self.outputs = (dict(self.outputs[0], text=str(text)),)
	text = property(None, set_text)
	


class Wellness:
	''' translate a quality metric into a colored status '''
	def __init__(self, levels, states):
		self.levels = levels
		self.states = states
	def gauge(self, value):
		op = float.__ge__
		count = len(list(keep(self.levels, lambda level: value >= level)))
		result = self.states[count]
		return result
	__call__ = gauge

wellness_state = Wellness([.01, .5, 1.], Strs('N/A Missing Partial Complete'))
wellness_color = Wellness([.01, .5, 1.], lseq(Strs('darkgrey indianred gold springgreen '), Color))



@dataclass
class Shaded:
	wellness: Wellness = wellness_color
	fmtr: callable = '<span style="color:{color}">{text:.2f}</span>'.format
	
	def gauge(self, value):
		wellness = self.wellness.gauge(value)
		result = self.fmtr(text=value, color=wellness)
		return result
	def igauge(self, values, to_seq=None):
		return xseq(values, to_seq, self.gauge)
ColorCode = Shaded

class ShadedItems(HTML, Shaded):
	
	def __init__(self, items=None, *args, **kargs):
		self.join = ('&nbsp;'*6).join
		self.items = list(items or [])
		Shaded.__init__(self, *args, **kargs)
		HTML.__init__(self, self.evaluate(), **kargs)
	def evaluate(self):
		# result = self.(**{k:getattr(self, k) for k, v in cls_vars(self) if k not in ('template')})
		result = self.igauge(self.items, self.join)
		return result
	def refresh(self):
		self.value = self.evaluate()
		
		
	items = PropertyReaction(refresh)


class ColorHints(HTML):
	_to_items = asis
	_to_color = None
	_to_html: callable = '<div style="display:inline-block; text-align: right; width: 40px; color:{color};">{item}</div>'.format
	_join = ''.join
	
	
	def __init__(self, items=None, to_items=None, to_color=None, to_html=None, join=None, **kargs):
		# self._items = list(items or [])
		self.to_items = to_items or self.__class__._to_items
		self.to_color = to_color or self.__class__._to_color
		self.to_html = to_html or self.__class__._to_html
		self.join = join or self.__class__._join
		HTML.__init__(self, self.evaluate(items), **kargs)
	def evaluate(self, items):
		items = self.to_items(items)
		items = [self.to_html(color=self.to_color(item), item=item) for item in items]
		result = self.join(items)
		return result
	def apply(self, items):
		# self._items = items
		self.value = self.evaluate(items)
		return self.value
	
	# properties
	items = property(lambda self: self._items, apply)
	
# Gradients
class ColorGauges(ColorHints):
	_to_color = wellness_color



''' Text UIs
'''

class ColorLabel(HTML):
	def __init__(self, text='hello world', color='lightblue', **kargs):
		self.template = Template('<span style="color:{{color}}">{{text}}</span>')
		self.text = text
		self.color = color
		super().__init__(self.gen_value(), **kargs)
	def gen_value(self):
		result = self.template.render(**{k:getattr(self, k) for k, v in cls_vars(self) if k not in ('template')})
		return result
	def refresh(self):
		self.value = self.gen_value()
		
	text = PropertyReaction(refresh)
	color = PropertyReaction(refresh)
	
	
class StrLabel(Label):
	to_str = str
	_layout = dict(width='auto')
	
	def __init__(self, obj='---', to_str=None, layout=None):
		layout = {**self._layout, **(layout or {})}
		super().__init__(layout=layout)
		
		to_str and setattr(self, 'to_str', to_str)
		self.obj = obj
	def update(self):
		self.value = self.to_str(self.obj)
		
	# properties
	obj = PropertyReaction(update)
	
class ReprLabel(StrLabel):
	to_str = str
	
PrintLabel = StrLabel


class StrText(Text):
	to_str = str
	_layout = dict(width='auto')
	
	def __init__(self, obj='---', to_str=None, layout=None):
		layout = {**self._layout, **(layout or {})}
		super().__init__(layout=layout)
		
		to_str and setattr(self, 'to_str', to_str)
		self.obj = obj
	def update(self):
		self.value = self.to_str(self.obj)
		
	# properties
	obj = PropertyReaction(update)


class StrTexts(GridCols):
	_ui_fab = StrText
	
class StrLabels(GridCols):
	_ui_fab = StrLabel
	


''' Selectors
'''

@dataclass
class MultiSelect(VBox):
	# fields
	options: list
	selected: list
	orient: str = 'horz'
	
	# properties
	selected_buttons = property(lambda self: keep(self.children, Name.value))
	selected = property(lambda self: lseq(self.selected_buttons, Name.description))
	
	def __post_init__(self):
		box_type = self.orient == Defs.horz and 'HBox' or 'VBox'
		self._model_name = box_type + 'Model'
		self._view_name = box_type + 'View'
		super().__init__([ToggleButton(description=option) for option in self.options])
		for child in self.children:
			child.observe(self.on_selection, 'value')
	def on_selection(self, change):
		change.owner.button_style = change.owner.value and 'success' or ''
	
# @dataclass
# class MultiSelect(VBox):
# 	# fields
# 	options = List()
# 	selected = List()
# 	orient: str = 'horz'
#
# 	# properties
# 	selected_buttons = property(lambda self: keep(self.children, Name.value))
# 	# selected = property(lambda self: lseq(self.selected_buttons, Name.description))
#
# 	def __post_init__(self):
# 		box_type = self.orient == Defs.horz and 'HBox' or 'VBox'
# 		self._model_name = box_type + 'Model'
# 		self._view_name = box_type + 'View'
# 		super().__init__([ToggleButton(description=option) for option in self.options])
# 		for child in self.children:
# 			child.observe(self.on_selection, 'value')
# 	def on_selection(self, change):
# 		change.owner.button_style = change.owner.value and 'success' or ''
# 		self.selection.select()


# class SelectionModel(Model, Selection):
# 	def set(self, *args, **kargs):			self.updating('set', *args, **kargs)
# 	def select(self, *args, **kargs):		self.updating('select', *args, **kargs)
# 	def deselect(self, *args, **kargs):		self.updating('deselect', *args, **kargs)
#

class MultiSelect(VBox):
	# fields
	orient: str = 'horz'
	
	# properties
	selected_buttons = property(lambda self: keep(self.children, Name.value))
	# selected = property(lambda self: lseq(self.selected_buttons, Name.description))
	
	btn_layout = dict(width='auto', min_width='70px')
	# btn_fab = lambda ToggleButton(select, description=option, layout=layout, button_style=select and 'success' or '')
	
	def __init__(self, selection):
		self.selection = selection
		
		box_type = self.orient == Defs.horz and 'HBox' or 'VBox'
		self._model_name = box_type + 'Model'
		self._view_name = box_type + 'View'
		super().__init__(
			[ToggleButton(select, description=option, layout=self.btn_layout, button_style=select and 'success' or '')
				for option, select in self.selection])
		
		for child in self.children:
			child.observe(self.on_selection, 'value')
	
	def on_selection(self, change):
		change.owner.button_style = change.owner.value and 'success' or ''
		self.selection.select(change.owner.description, enabled=change.owner.value)


	

import traceback as tb

class JArgsUI(Box):
	_anno_uifab_map = dict(
		bool		= Checkbox,
		str			= Text,
		int			= IntText,
		float		= FloatText,
		datetime	= DatePicker,
		dateclock	= DatePicker,
		DEFAULT		= Text,
	)
	_layout = Defs.xy_Ayz_aaa
	
	def __init__(self, func, num_pargs=0, layout=None):
		self.func = func
		self.num_pargs = num_pargs
		self.arg_seq = inspect.getfullargspec(func).args
		self.uis = self.get_uis(func=self.func)
		layout = {**self._layout, **(layout or {})}
		super().__init__([Label(func.__name__)]+self.uis, layout=layout)
	@classmethod
	def get_ui(cls, arg_type, default=nothing):
		ui_fab = typeof(arg_type, [Widget]) and arg_type		# check if is Widget
		ui_fab = ui_fab or cls._anno_uifab_map.get(arg_type)	# else check mapped type
		ui_fab = ui_fab or (									# else check mapped type name
				typeof(arg_type, type) and cls._anno_uifab_map.get(arg_type.__name__))
			
		# if any of above then create ui from ui_fab
		ui = None
		if ui_fab:
			try:
				assert default is not nothing, ''
				ui = ui_fab(default)
			except:
				ui = ui_fab()
		
		return ui
	@classmethod
	def get_uis(cls, annotations=None, func=None, defaults=None):
		# print(inspect.getfullargspec(func))
		annotations = annotations if annotations is not None else func.__annotations__
		argspec = inspect.getfullargspec(func)
		if func:
			arg_seq = argspec.args
			annotations = annotations if not func else imap(arg_seq, annotations, to_item=True, on_miss='continue')
		
		# ascertain ui fabricators based on each annotation
		named_uis = []
		no_default_len = min(0, len(argspec.args)-len(argspec.defaults))
		defaults = lreps([nothing, argspec.defaults], [no_default_len, '*'])
		# defaults = FullMap(nothing, zip(argspec.args[len(argspec.defaults)], argspec.defaults))
		for (arg_name, arg_type), default in zip(annotations, defaults):
			ui = cls.get_ui(arg_type, default=default)
			if ui:
				ui.description = arg_name
				named_uis.append(ui)
		return named_uis
	def get_args(self, num_pargs=None):
		num_pargs = self.num_pargs if num_pargs is None else num_pargs
		
		pa, ka = [], {}
		return pa, ka
	def __call__(self, *pa, missing=ValueError, **ka):
		try:
			# generate an arg_value for each func position arg given by arg_uis overriden with given *pa, **ka
			arg_ui_map = dict([[child.description, child] for child in self.children])
			pa_value_map = dict([name_value for name_value in izip(self.arg_seq, pa, fill='missing')])
			argui_value_map = dict([[child.description, child.value] for child in self.children])
			ftee(locals(), '{ka} {pa}')
			ftee(locals(), '{argui_value_map} {pa_value_map}')
			arg_values = []
			
			
			for arg_name in self.arg_seq:
				# arg_value resolves to an available source with top priority: 1. ka, 2. pa (if non-nothing), 3. arg_ui
				if arg_name in ka:
					arg_value = ka.pop(arg_name)
				elif arg_name in pa_value_map and pa_value_map[arg_name] != 'missing':
					arg_value = pa_value_map[arg_name]
				elif arg_name in arg_ui_map:
					arg_value = argui_value_map[arg_name]
				else:
					raise KeyError(f'Missing arg for {arg_name!r} from all sources. This could be a bug.')
				
				if typeof(arg_value, str):
					arg_value = arg_value.encode().decode('unicode_escape')
				arg_values.append(arg_value)
			
			# generate error for missing args
			if typeof(missing, Exception) and nothing in arg_values:
				missing = [a_name for a_name, a_val in zip(self.args_seq, arg_values) if a_val is nothing]
				raise ValueError(f'Missing args {missing}')
			
			# invoke func with arg_values and return any result
			ftee(locals(), '{arg_values} {ka}')
			
			func = self.func
			result = func(*arg_values, **ka)
		except:
			# exc_type, exc_value, exc_traceback = sys.exc_info()
			result = tb.format_exc()
		return result
	
	
	# properties
	pargs = property()
	kargs = property()
	
		
	
