
import sys

import os
import datetime
import atexit
import colorlover as cl
from filelock import FileLock, Timeout
from plotly import plotly as pl, tools as tls, graph_objs as go
from time import time
from pyutils.patterns.idfactory import *
from pyutils.patterns.decor import *
from pyutils.common.thread import *
from pyutils.extend.datastructures import XObj
Stream = PlotlyStream = XObj


class DataPoint(object):
	def __init__(self, y, x=None):
		self.x, self.y = x, y
class DataPoints(object):
	''' must resolve to a number or numeric list
		Intended to be plotted as a series on a graph
	'''
	plurality_types = (tuple, list)
	numeric_types = (int, float)
	@staticmethod
	def from_repr(obj):
		if not hasattr(obj, '__len__'):
			return [DataPoint(obj)]										# data is a individual datapoint representations
		if not hasattr(obj[0], '__len__'):
			return [DataPoint(point) for point in obj]					# data is a list of individual datapoint representations
		if isinstance(obj[0], DataPoint):
			return obj													# data is a list of datapoint instances; no change needed
		if isinstance(obj, dict) or (not isinstance(obj, dict) and len(obj[0]) == 2):
			return [DataPoint(dict(point)) for point in obj]			# data is a dict datapoint format

		raise TypeError('Error: Unexpected obj %s for DataPoint. obj must be number or number list' % obj)
	@staticmethod
	def get_format(data_points):
		if data_points is None or len(data_points) == 0:
			return 0
		return len(data_points)
		# last_item = data_points[-1]
		# if isinstance(last_item, DataPoints.plurality_types):
		# 	data_format = (type(last_item),) + tuple(type(item) for item in last_item)
		# elif isinstance(last_item, DataPoints.numeric_types):
		# 	data_format = type(last_item)
		# else:


class DataSeries(object):
	def __init__(self, data_points_list=[]):
		self.data_points_list = data_points_list
		self.series_format = None
		# self.check_format(data_points_list or None, enforce_valid=False)

	def check_format(self, data_points, enforce_valid=True):
		data_format = DataPoints.get_format(data_points)

		self.series_format = self.series_format or data_format
		if self.series_format != data_format:
			raise TypeError('Series format %s does not match new data format %s.' % (self.series_format, data_format))

		if enforce_valid and not self.series_format:
			raise TypeError('Invalid data provided %s.' % data_points)

		return self.series_format

	def __iadd__(self, data_points):
		result = DataPoints.from_repr(data_points)
		self.check_format(result)
		self.data_points_list.append(result)
		return self
	def __iter__(self):
		return self.data_points_list



class minGraphConfig(dict):
	def __init__(self, **kwargs):
		''' constructor '''
		dict.__init__(self, **kwargs)
class minGraphConfigData(minGraphConfig):
	def __init__(self, index, **kwargs):
		''' constructor '''
		self.index = index
		minGraphConfig.__init__(self, **kwargs)
	@staticmethod
	def from_list_repr(obj_list):
		''' produces a list of minGraphConfigData object given a list of dicts '''
		if isinstance(obj_list, (list, tuple)):
			result = [minGraphConfigData(index, **obj) for index, obj in enumerate(obj_list)]
			return result

		raise TypeError()
class minGraphConfigLayout(minGraphConfig): pass

class minGraph(IdFactory):
	instances = {}
	def __init__(self, id, data_points=None):
		''' constructor '''
		if hasattr(self, 'id'): return

		self.series = None
		self.streams = []
		self.streaming_timeout_secs = 20 # 10 * 60; fixme: ability to finalize promptly on termination before extending timeout to 10 mins
		self.close_session_time = None
		self.timer = None
		self.plot_url = None
		self.layout_config, self.data_config_list = minGraphConfigLayout(), {}
		super(minGraph, self).__init__(id)
		atexit.register(self.__del__)

	def __del__(self):
		''' close existing sessions on termination '''
		# fixme: atexit.register(self.__del__) does not seem to force a call to this on exit
		self.close_on_idle(force_close=True)
		for stream in self.streams:
			stream.lock.release()

	def reserve_streams(self):
		''' initialize data structures needed for a plotly graph
		 	prerequisites:
		 		1. provision needed stream tokens by going to https://plot.ly/settings/api
		 		2. manually insert tokens as api streams_ids into credentials file located at ~/.plotly/.credentials
		'''

		# read plotly credentials file for stream_ids
		credentials = tls.get_credentials_file()
		lock_dir = os.path.dirname(tls.CREDENTIALS_FILE) + '/stream_locks/'
		os.path.isdir(lock_dir) or os.makedirs(lock_dir)

		stream_id_lock_list = []

		# lock total required stream ids
		for stream_id in credentials['stream_ids']:
			stream_lock = FileLock(lock_dir + '/' + stream_id)
			try:				stream_lock.acquire(timeout=0)
			except Timeout:		continue

			# thread & process concurrency lock acquired for stream_id
			stream_id_lock_list.append((stream_id, stream_lock))
			if len(stream_id_lock_list) == self.series.series_format:
				self.streams = [Stream(plstream=PlotlyStream(token=id, maxpoints=80), lock=lock)
								for id, lock in stream_id_lock_list]
				return

		# loop exited before locks could be acquired for all stream; release locks and raise error
		for _, lock in stream_id_lock_list:
			lock.release()
		raise Exception('Failed to reserve enough plotly stream ids.  free: %d  needed: %d'
						% (len(stream_id_lock_list), self.series.series_format))

	def configure_plotly(self):
		''' configure a plotly graph given some sample data '''
		# todo: generalization and customization

		# define figure; layout and data
		colors = cl.scales[str(self.series.series_format)]['qual']['Set1']
		self.figure = go.Figure(
			layout=go.Layout(
				title='Chimes Debug',
				yaxis={'title':'y for trace1',
						"anchor": "free", "autorange": True,"type": "linear","gridcolor": "#E1E5ED","titlefont": {"color": "#4D5663"}}
				# yaxis2=dict(
				# 	title='y for trace2',
				# 	titlefont=dict(color='rgb(148, 103, 189)'),
				# 	tickfont=dict(color='rgb(148, 103, 189)'),
				# 	overlaying='y',
				# 	side='right'
				# )
			),
			data=[	go.Scatter(name='trace%s'%index, stream=stream.plstream.__dict__, x=[], y=[], yaxis='y' if index != 4 else 'y2', marker=dict(color=col))
					for index, (stream, col) in enumerate(zip(self.streams, colors)[:self.series.series_format]) ],
		)

		#
		self.figure.layout.update(self.layout_config)
		for data in self.data_config_list.values():
			# modify data series as configured
			self.figure.data[data.index].update(data)

			# ensure layout has yaxis specified by data series; create if missing
			if 'yaxis' in data:
				yaxis_key = data['yaxis'].replace('y', 'yaxis').replace('1', '')
				if yaxis_key not in self.figure.layout:
					self.figure.layout[yaxis_key] = {'title':data['name'], "anchor": "free", "autorange": True,"type": "linear","gridcolor": "#E1E5ED","titlefont": {"color": "#4D5663"},}

		#
		yaxes = [key for key in self.figure.layout.keys() if key.startswith('yaxis')]
		yaxes.sort()
		count = len(yaxes)
		avg_size = 1.0/float(count)
		for index, yaxis in enumerate(yaxes):
			self.figure.layout[yaxis]['domain'] = [1-(index*avg_size)-avg_size, 1-(index*avg_size)-.02]

		# configure graph on plotly server
		print('creating plotly graph file: %s' % self.id)
		self.plot_url = pl.plot(self.figure, filename=self.id)

	def clear(self):
		# todo:
		pass

	def configure(self, config):
		''' Graph obj schema can be found '/home/zeroone/.local/lib/python2.7/site-packages/plotly/package_data/default-schema.json' '''
		if isinstance(config, list):
			for graph_config in config:
				self.configure(graph_config)
		elif isinstance(config, minGraphConfigLayout):
			self.layout_config = config
		elif isinstance(config, minGraphConfigData):
			self.data_config_list[config.index] = config
		else:
			raise TypeError()

		if self.plot_url:
			self.configure_plotly()

	@staticmethod_optional_self
	def __iadd__(self, data_points):
		''' Update graph with new points to respective streams
			If called as class method data_points must be a dict of data_points.
		 	If called as instance method input must be in data_points format.
			Usages:
		 	class method: 		minGraph += {'GraphId0': data_points, ...}
		 	instance method: 	minGraph('GraphId0') += data_points
		'''
		# todo: thread safety
		# handle class method invocation
		if self is None:
			results = []
			for graph_id, graph_data_points in dict(data_points).items():
				if not graph_id in minGraph.instances:
					minGraph.instances[graph_id] = minGraph(graph_id)
				minGraph.instances[graph_id] += graph_data_points
				results.append(minGraph.instances[graph_id])

		# handle instance method invocation
		else:
			# firstly append data_points to series to ensure series has a data format
			data_points = DataPoints.from_repr(data_points)
			self.series = self.series or DataSeries()
			self.series += data_points

			# is initial data_points? then configure graph and reserve plotly streams in case of concurrency
			if not self.streams:
				self.reserve_streams()
				self.configure_plotly()

			# handle session state
			sessions_created = self.close_session_time
			if not sessions_created: print('opening plotly stream to %s' % self.id)
			self.close_session_time = int(time()) + self.streaming_timeout_secs
			self.close_on_idle()

			# write each data_point to its stream
			try:
				x = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
				for data_point, stream in zip(list(data_points), self.streams):
					if not sessions_created:
						stream.session = pl.Stream(stream_id=stream.plstream.token)
						stream.session.open()

					data_point.x = data_point.x or x
					stream.session.write(data_point.__dict__)
			except Exception as e:
				print('mingraph.__iadd__(): %s' % e)

			return self

	def close_on_idle(self, force_close=False):
		''' close plotly session '''
		# exit condition: timer instance already counting down or no available session to close
		if self.timer or not self.close_session_time:
			return

		# idle timeout not reached?; then check again later; else close stream sessions
		if not force_close and int(time()) < self.close_session_time:
			self.timer = timer(self.close_on_idle, self.close_session_time - int(time()) + 1)
		else:
			print('closing plotly stream to %s' % self.id)
			self.close_session_time = None
			self.timer = None
			for stream in self.streams:
				stream.session.close()

		pass
