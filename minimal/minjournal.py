#!/usr/bin/env python
import sys
import fcntl
import os
import signal
import re
from time import sleep
from pathlib import Path
from subprocess import Popen,PIPE

from pykit.utilities.analyzeutils import ffile
from pyutils.defs.chardefs import ctrl
from pyutils.structures.maps import Vars
from pyutils.utilities.maputils import *
from pyutils.utilities.iterutils import *
from pyutils.utilities.callutils import *

Path.str = property(str)


#region journal defines
class DefsJournal:
	#region path defs
	p_py_local			= ffile()
	p_bin				= Path('/home/zeroone/anaconda3/envs/checkout/bin')
	p_py				= p_bin / 'python'
	p_jcli				= p_bin / 'jupyter-console'
	
	exts_p_py=ext_p_py,	= '.py',
	rxc_p_py			= re.compile(r'^[/\w:.\-%]{1,200}$')
	#endregion
	#region cli defs
	flags				= '''	--simple-prompt
								--ZMQTerminalInteractiveShell.banner=""
								--ZMQTerminalIPythonApp.confirm_exit=False'''.replace('\t','').split()
	s_did_finish		= ctrl.d.hex  # send EOF to stdin to end jlab session keeping kernel alive
	ns_did_finish		= len(ctrl.d.hex)
	cmd_magic_run_of	= lambda py,time=0,iact=1: '%run {} {!s}\n'.format((time and ' -t' or '') + (iact and ' -i' or ''),py)
	rxc_prompt			= re.compile(r'^\nIn \[\d+\]: ')
	rxc_prompt_end		= re.compile(r'\nIn \[\d+\]: $')
	#endregion
	#region instruct defs
	kjnl_fill			= '--existing'
	cmd_py_at			= Vars(tomap(to_value=lambda s: s.replace('\t',''),
							exec_pyargs = '''
								import sys
								sys.argv = {0!r}
								exec(open({0[0]!r}).read())
							''',
							io_stdlib_sleep = '''
								from time import sleep
								b = 2
								print('one')
								sleep(4)
								print('two')
							''',
							io_impsrc_persist = '''
								from minimal.minimal.minjournal import print_product,step
								
								show_2x2x3()
								show_steps()
								''' ))
	#endregion
Def						= DefsJournal
#endregion

#region prereq utils
def import_updated(): pass		# todo:
def import_modified(): pass		# todo:
def import_once(): pass			# todo:

def make_nonblocking(*fstreams):
	for fstream in fstreams:
		fd = fd if type(fstream) is int else fstream.fileno()
		fl = fcntl.fcntl(fd, fcntl.F_GETFL)
		fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)  # todo: coroutine loop.connect_read_pipe(protocol_factory, pipe)
#endregion

#region journal utils
def hook_on_finish(vers='v0.1.1',ipy_evt='post_run_cell'):
	import IPython
	
	def on_finish(*_):
		sys.stderr.write(Def.s_did_finish) # send EOF to stdin to end jlab session keeping kernel alive
	on_finish.vers = vers
	
	# hook callback
	cb,ipy = None,IPython.get_ipython()
	for cb in ipy.events.callbacks[ipy_evt]:
		if 'on_finish' != cb.__qualname__: continue
		if cb.vers == vers: continue
		ipy.events.unregister(ipy_evt,cb)
	cb or ipy.events.register(ipy_evt,on_finish) or print('hooked on_finish() version:%s' % vers)
def omit_prompt(output): return Def.rxc_prompt_end.sub('',Def.rxc_prompt.sub('',output))  # todo: find a better way


def cmd_conn_of(kjnl=None):
	cmd_conn = (Def.p_py.str,Def.p_jcli.str,  *Def.flags,  kjnl or Def.kjnl_fill)
	return cmd_conn
def cmd_hook_of():
	# fixme: detect current hook version and reload only when needed
	cmd_hook = "from minimal.minimal.minjournal import hook_on_finish;hook_on_finish()\n"
	return cmd_hook
def cmd_py_of(py=None,*args,time=False):
	if py.endswith(Def.ext_p_py) and Def.rxc_p_py.fullmatch(py):
		py = Path(py).absolute()
		cmd_py = Def.cmd_magic_run_of(py.str,time)
	else:
		cmd_py = Def.cmd_py_at.get(py,py)
	return cmd_py


def run_py_at(py=Def.p_py_local,kjnl=None,latency=.5):
	''' run python source *py* at *jkey* target jlab kernel instance
		:param py:
		:param jkey:
		:param latency:
	'''
	# todo: this should be able to target a known juncture
	# todo: implement parameterized juncture operations
	# resolve inputs
	cmd_conn = cmd_conn_of(kjnl)
	cmd_hook = cmd_hook_of()
	cmd_py = cmd_py_of(py,time=True)
	# return
	
	# execute commands
	proc = Popen(cmd_conn,stdin=PIPE,stdout=PIPE,stderr=PIPE)
	make_nonblocking(proc.stdout,proc.stderr)
	
	# instruct jlab
	# proc.stdin.write(cmd_hook.encode()); sleep(1)
	# todo: start performance trackers for named task
	proc.stdin.write(cmd_py.encode())
	proc.stdin.flush()
	
	# poll io for finish msg
	did_finish = False
	while not did_finish:
		stdout,stderr = proc.stdout.read(),proc.stderr.read()
		if None is stdout is stderr:
			sleep(latency)
		else:
			# did_finish,stderr = stderr and stderr.decode().endswith(Def.s_did_finish) and (,stderr) or (stderr[])
			if stderr and stderr.decode().endswith(Def.s_did_finish):
				did_finish,stderr = (True,stderr[:-(Def.ns_did_finish+1)])
			stdout and sys.stdout.write(omit_prompt(stdout.decode()))
			stderr and sys.stderr.write(stderr.decode())
			did_finish and proc.stdin.close()
	
	stdout,stderr = proc.stdout.read(),proc.stderr.read()
	stdout and sys.stdout.write(omit_prompt(stdout.decode()))
	stderr and sys.stderr.write(stderr.decode())
#endregion

#region journal devices
class Preset:
	pass
preset_at = Vars(
	autoreload=...,
	on_finish=...,
	time=...,
)
class Journal:
	hooks: strs			= ()
	presets: strs		= ()
	# routines| prep job script history env contexts series build instructions recipe ops steps setup presets
	
class Journals:
	# todo:
	#  tasking: start_new start_workspace stop save
	#  multitasking: migrate fork patch
	#  configure: set_default autosave
	#  jnl~junc: user may specify any of:  jlab, juncture, both or neither
	def save(self): pass
#endregion

#region local tests
def show_product(msgs_left, *msgs_right, left='',sep=' ',interval=.25):
	''' a toy program to test io over time '''
	from time import sleep
	for msg in msgs_left:
		if msgs_right:
			show_product(*msgs_right, left=left+msg+sep, sep=sep)
		else:
			sleep(interval)
			print(left + msg)
show_2x2x3 = stage_call(show_product,(':$',':#'), 'ab', '123')

def show_steps(steps=4,count_attr='tally',most=5,interval=.25):
	''' a toy program to test io over time and variable persistence '''
	from time import sleep
	print()
	print(count_attr,globals().get(count_attr),sep=' = ')
	for i in range(steps or 1):
		sleep(interval)
		count = globals()[count_attr] = (globals().get(count_attr,0)) % most + 1
		print('.'*(count))
	print(count_attr,globals().get(count_attr),sep=' = ')
	return count

def show_all():
	show_2x2x3()
	show_steps()


if __name__=='__main__':
	from pyutils.output.colorize import pyellowl
	
	# pyellowl(sys.argv)
	script,do,*pa = *sys.argv,None
	actions_show = hss('all  product  steps  2x2x3 ')
	out = (
		globals()['show_%s'%do]()	if do in actions_show else
		run_py_at(*pa)				if do == 'py' else
		run_py_at(do)				if do.endswith('.py') else
		throw(f'Unrecognized args: %s'%pa)
	)
#endregion

#region scratch area
# journals junctures perpetuals residuals proceeds transcripts trials replicas artifacts course preserves board
# footsteps


'''
# start jupyter lab server
/home/zeroone/anaconda3/envs/checkout/bin/jupyter-lab --config=/home/zeroone/.jupyter/jupyter_notebook_config.py --ip=192.168.1.126

# start jupyter console instance
/home/zeroone/anaconda3/envs/checkout/bin/python /home/zeroone/anaconda3/envs/checkout/bin/jupyter-console --ZMQTerminalInteractiveShell.banner='' --existing

/home/zeroone/workspace/checkout/lib/minimal/test/addt/jpy_set_now.py
/home/zeroone/workspace/checkout/lib/minimal/test/addt/jpy_data_libs.py
'''
#endregion
