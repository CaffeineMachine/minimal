from __future__ import print_function
from TkinterDnD2 import *
import types
import Tkinter as tk
import Tix as Tix
import ttk
import Pmw
import re
from collections import defaultdict as ddict
from pyutils.extend.datastructures import XDict
from pprint import pprint as pp
from collapsibleframe.collapsibleframe import *
from pyutils.common.log	import *


class Config:
	''' serves as a template with custom attribute defaults '''
	standard = dict(pack=dict(expand='1', fill='both'))

	def __init__(self, attr={}, pack={}, **kwargs):
		''' construct '''
		self.__dict__.update(XDict(locals()) ^ ['self'])
		self.pack = XDict(Config.standard['pack']) | self.pack

class Prototype:
	''' a set of template custom config attribute defaults '''
	standard = Config(pack=dict(expand='1', fill='both'))
	default = ddict(lambda: Config())

	def __init__(self, theme_id=None, configs={}):
		''' construct '''
		context = Prototype.default
		if theme_id is not None:
			Prototype.default.setdefault(theme_id, ddict(lambda: Config(pack=dict(expand='1', fill='both'))))
			context = Prototype.default[theme_id]

		context.update(configs)


class mintk:
	widget_sequence = 1
	named_widgets = ddict(lambda: ddict(list))
	def __init__(self, widget_class=TkinterDnD.Tk, **kwargs):
		''' construct '''
		self.widget_class = widget_class
		self.kwargs = self.set_layout(**kwargs)
		self.sequence = mintk.widget_sequence
		mintk.widget_sequence += 1

	def set_layout(self, **kwargs):
		self.rtpack = XDict(kwargs.get('rtpack', {}))		# rtpack: (root pack) differs from pack by being inherited as new default by subwidgets
		self.pack = (XDict(self.rtpack) | Prototype.default[self.widget_class].pack) | kwargs.get('pack', {})
		self.attr = XDict(Prototype.default[self.widget_class].attr) | kwargs.get('attr', {})
		self.pack_private = kwargs.get('pack_private', {})
		# todo: interpret grid parameters
		kwargs = XDict(kwargs) ^ ['pack', 'grid', 'pad', 'attr']
		return kwargs

	@staticmethod
	def get(name, hint='unspecified', index=0, assertion=False):
		if name not in mintk.named_widgets:
			if not assertion:
				return None
			raise KeyError('No widget named %s' % name)


		results = mintk.named_widgets.get(name, None)  # get all widgets with given name
		if hint == 'unspecified':
			result = results.values()[0]
		elif hint in results:
			result = results[hint]
		else:
			results = [result for key,result in results.items() if hint in key]
			result = results[0]
		# hint = hint is 'unspecified' and results.keys()[0] or hint  # if unspecified get 1st key
		# trace('name,tree,index = %s,%s,%s: ' % (name, hint, index), end='')
		# pp(results)
		# result = results[hint]  # select widget under specified tree with given index
		pp(result)
		result = result[index]  # select widget under specified tree with given index
		trace(result)

		return result

	def instantiate(self, attr={}, rtpack={}):
		trace()
		# widget_class = len(self.args) and self.args[0] or TkinterDnD.Tk
		if isinstance(self.widget_class, tk.Misc):
			self.widget = parent = self.widget_class
		else:
			root_arg = 'parent' in self.attr and [self.attr['parent']] or []
			root_arg = 'parent' in attr and [attr['parent']] or root_arg
			if len(root_arg) and isinstance(root_arg[0], CollapsibleFrame):
				root_arg = [root_arg[0].interior]
			kwattr = (XDict(self.attr) | attr) ^ ['parent', 'parent_names']
			kwattr = issubclass(self.widget_class, tk.Misc) and kwattr or XDict(kwattr) ^ ['name']
			'name' in kwattr and trace('name: %s' % kwattr['name'])
			self.widget = parent = self.widget_class(*root_arg, **kwattr)
		# trace('%s()'%str(type(self.widget)))

		if isinstance(self.widget, tk.Tk):
			trace('Pmw.initialise(%s)'%str(self.widget_class))
			Pmw.initialise(self.widget)
		else:
			# todo: refactor/simplify this block
			pack = rtpack.copy()
			pack.update(self.pack) # null-coalescing operation; select 1st non-null
			pack = XDict(pack) | self.pack_private # combine pack_private settings; prefer pack_private for conflict
			trace('packing %s: %s' % (attr.get('name'), pack))
			self.widget.pack(**pack)
			# todo: configure grid parameters

		# set parant_name to full string of parents up to the root widget
		self.parent_names, self.name = attr.get('parent_names'), attr.get('name', 'root')
		trace(self.name+': ', end=''); trace(self.parent_names)
		mintk.named_widgets[self.name][self.parent_names].append(self.widget)

		# maintain definition sequence order
		subwidgets = sorted(self.kwargs.items(), key=lambda (name, widget): widget.sequence)

		for name, arg in subwidgets:
			# integration of py mega widgets notebook tabs
			# this pattern stipulates new tab is given as parent to child
			if isinstance(self.widget, Pmw.NoteBook):
				parent = self.widget.add(name)
			if isinstance(self.widget, Tix.NoteBook):
				parent = self.widget.add(name, label=name)


			# prepare child; if arg is a widget type rather than instance define child as instance of type
			if isinstance(arg, types.ClassType):
				arg = mintk(arg)
			parent_names = (self.parent_names and self.parent_names or ()) + (self.name,)
			child = arg.instantiate(attr=dict(parent=parent, parent_names=parent_names, name=name), rtpack=self.rtpack)

			# integration of themed Tk notebook tabs
			# this pattern stipulates notebook is given as parent to child and child is added as new tab
			if isinstance(self.widget, ttk.Notebook):
				self.widget.add(child, text=name)

		return self.widget


