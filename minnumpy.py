import numpy as _np
from numpy import *
from pyutils.defs.funcdefs import noop

#region minnumpy utils
def truth_of_coll(coll):
	''' universally understood and *expected* collection truth test '''
	truth = bool(len(coll))
	return truth

def xyx_of_box(box, makes=None, make=None):
	''' get ordered indices for an np.array with given *box* as shape
		:param box: start/stop ranges for each axis; accepts an iterable or ints or slices
		:param makes: coerce each coord if given
		:param make: coerce coords if given
		
		usage:
		grid_coords([3, 3])             # => [[0,0], [0,1], [0,2], [1,0], ... [2,2]]
		grid_coords([3, slice(10,13)])  # => [[0,10], [0,11], [0,12], [1,10], ... [2,12]]
	'''
	# resolve inputs
	# make = make or (makes and np.asarray)
	
	# coerce box to slice ranges
	# todo: convert to function; RELO to pykit.devices.nums
	box = list(box)
	for idx,bound in enumerate(box):
		if isinstance(bound, int):
			box[idx] = slice(0,bound)
		elif not type(bound) is slice:
			raise TypeError()
	
	# generate xyx|coords:  box => xyx
	xyx = np.moveaxis(np.mgrid[box], 0, -1)  			# grid of coords like [[[z y x], ...], ...]
	xyx = xyx.reshape(xyx.shape[0]*xyx.shape[1], -1)	# list of coords [[z y x], ...]
	xyx = xyx if not makes else map(makes,xyx)
	xyx = xyx if not make else make(xyx)
	return xyx

def _ndarray_generators(func_names):
	''' given a set of ndarray creation functions as a list of strings
		assign a xndarray wrapper function to this module's global scope
	'''
	for func_name in func_names:
		np_func = getattr(_np, func_name)
		globals()[func_name] = (lambda func: lambda *args, **kws: ndarray(func(*args, **kws)))(np_func)
		
		
coords_of_box							= xyx_of_box
#endregion

#region minnumpy devices
class ndarray(ndarray):    # todo: refactor this in the same fashion as minpandas
	''' bool() defective in ndarray for precaution concerns
		Produces:
		ValueError: The truth value of an array with more than one element is ambiguous. Use a.any() or a.all()
		Actually bool(iterable) is generally interpreted as bool(len(iterable)) to so thanks but no thanks
	'''
	def __new__(cls, input_array):
		obj = asarray(input_array).view(cls)
		return obj
	
	def __bool__(self):
		result = bool(len(self))
		return result
		
	# method alias
	__nonzero__ = __bool__
xndarray = ndarray  # compatibility aliases

# numpy function defined temporarily for intellisence awareness
zeros,ones,full,arange,linspace = (noop,)*5
_ndarray_generators('zeros,ones,full,arange,linspace'.split(','))  # todo: refactor and get rid of this
#endregion

#region inline test
if __name__=='__main__':
	from time import sleep
	
	# initialization
	xarr = zeros(5)
	first_if_true = True and xarr or []
	second_if_false = False and xarr or []
	
	# results
	print(xarr)             # => [ 0.  0.  0.  0.  0.]
	print(bool(xarr))       # => True
	print(first_if_true)    # => [ 0.  0.  0.  0.  0.]
	print(second_if_false)  # => []
	sleep(.1)
	
	# the following results in the exception:
	# ValueError: The truth value of an array with more than one element is ambiguous. Use a.any() or a.all()
	error = True and np.zeros(5) or []
#endregion
