import urwid
from pyutils.patterns.idfactory import *

from collections import namedtuple

from collections import Counter

import urwid as wid
from urwid import *
from urwid.util import is_mouse_press
from urwid.split_repr import python3_repr
from pyutils.structures.units import *
from dataclasses import dataclass, field


__all__ = [
	'DefsMinurwid', 'MinText', 'MinEdit', 'MinButton', 'MinPicker', 'DeltaTimePicker',
	'MinCheckBox', 'Accordian', 'LogView', 'DequeWalker', 'SingleLogView']

import sys
log = sys.gettrace() and print or None

class DefsMinurwid:
	key_ind, value_ind = range(2)
Defs = DefsMinurwid


class IdentityInds:
	def __getitem__(self, ind):
		result = list(range(*ind.indices(len(self)))) if isinstance(ind, slice) else ind
		return result
	def __len__(self):				return sys.maxsize
	def __iter__(self):
		for value in range(len(self)):
			if value > 10000:
				raise IndexError()
			yield value
	
	
@dataclass
class Enum:
	enum: 'types.Any' = field(default_factory=IdentityInds)  # defaults to returning given index
	index: int = 0
	itr_range: 'types.Any' = None
	
	def __post_init__(self):
		if not hasattr(self.enum, '__getitem__'):
			self.enum = list(self.enum)
		return
	@property
	def value(self):			return self.enum[self.index]
	def __len__(self): return len(self.enum)
	def get_index(self, offset=0):
		if isinstance(offset, slice):
			offset = slice(*offset.indices(len(self.enum)))
			index = slice(offset.start+self.index, offset.stop+self.index, offset.step)
		else:
			index = offset + self.index
		result = self.enum[index]
		return result
	def nget_index(self, offset=0):	return self.get(-offset)
	def set_index(self, index):
		self.index = index
		result = self.enum[self.index]
		return result
	def set_value(self, value, *list_start_stop, getter=None):
		values = self.enum if getter is None else [getter(item) for item in self.enum]
		self.index = values.index(value, *list_start_stop)
		result = self.enum[self.index]
		return result
	def __iadd__(self, offset, error=True):
		index = self.index + offset
		if 0 <= index < len(self):
			self.index = index
		elif error:
			raise IndexError()
		return self
	def __isub__(self, offset):	return self.__iadd__(-offset)
	def next(self, offset=1, error=True):
		try:
			self += offset
			return self.value
		except IndexError as e:
			if error: raise StopIteration()
		return self.value
	def prev(self, offset=1, error=True):	return self.next(-offset, error=error)
	def items(self):			return enumerate(self.enum)
	def keys(self):				return range(len(self.enum))
	def values(self): return iter(self.enum)
	# method aliases
	__getitem__ = __add__ = __radd__ = get_index
	__sub__ = nget_index
	__next__ = next
	__iter__ = values
	

class MinText(wid.Text):
	text = property(lambda self:self.get_text()[0], wid.Text.set_text,
		doc="""Read-only property returning the complete bytes/unicode content of this widget""")
	# def rows(self, size, focus=False): return 1
	
class MinEdit(wid.Edit):
	text = property(wid.Edit.get_edit_text, wid.Edit.set_edit_text,
		doc="""Read-only property returning the complete bytes/unicode content of this widget""")
	
	
class MinButton(wid.Columns):
	_sizing = frozenset([wid.FLOW])
	signals = ["click"]

	def __init__(self, label, pfx_sfx=('[',']'), on_press=None, user_data=None, dividechars=0, **kws):
		self.label_part = wid.SelectableIcon(label, 0) if isinstance(label, str) else label
		self.sized_label = self.label_part if isinstance(self.label_part, tuple) else ('fixed', len(self.label_part.text), self.label_part)
		
		super().__init__(
			(pfx_sfx[0] and [('fixed', len(pfx_sfx[0]), Text(pfx_sfx[0]))] or []) +
			[self.sized_label] +
			(pfx_sfx[1] and [('fixed', len(pfx_sfx[1]), Text(pfx_sfx[1]))] or []),
			dividechars=dividechars, **kws)

		if on_press:
			connect_signal(self, 'click', on_press, user_data)
	# text = property(lambda w: w.label_part.text,lambda w, text: w.label_part.set_text(text))
	@property
	def text(self): return self.label_part.text
	@text.setter
	def text(self, text):
		self.label_part.set_text(text)
	def pack(self, size, focus=False):
		widths = self.column_widths(size, focus)
		columns = sum(widths)
		rows = self.rows(size, focus)
		log and log(f'MinButton({self.text}).pack: {(widths, )}')
		return columns,
	def keypress(self, size, key):
		if self._command_map[key] != ACTIVATE:
			return key

		self._emit('click')
	def mouse_event(self, size, event, button, x, y, focus):
		if button != 1 or not is_mouse_press(event):
			return False

		self._emit('click')
		return True
	
	
KeyValue = namedtuple('KeyValue', 'key value')
class MinPicker(wid.Columns):
	_sizing = frozenset([wid.FLOW])
	signals = ['change', 'postchange']
	kv_adapters = dict()
	_as_item = lambda item: KeyValue(key=item, value=item)
	_as_tuple = lambda item: KeyValue(key=item[0], value=item[1])

	def __init__(self, itr=None, down_up=('-','+'), to_kv=_as_item, user_data=None, dividechars=0, **kws):
		self.itr = itr and Enum(itr) or Enum()
		self.to_kv = to_kv
		self._key, self._value = self.to_kv(self.itr.value)
		self.label = MinText(str(self.key), wrap=wid.CLIP, align=wid.RIGHT)
		self.sized_label = ('fixed', self.max_width(), self.label)
		self.decr_button, self.incr_button = MinButton(down_up[0], ('', '')), MinButton(down_up[1], ('', ''))
		
		super().__init__(
			[(wid.PACK, self.decr_button), self.sized_label, (wid.PACK, self.incr_button)],
			dividechars=dividechars, **kws)

		connect_signal(self.incr_button, 'click', self.incr)
		connect_signal(self.decr_button, 'click', self.decr)
	checked = property(
		lambda self: self.label.get_text(),
		lambda self, text: self.label,
	)
	def set_key(self, key, signal=False):
		if signal:
			self._emit('change', self._value)
		value = self.itr.set_value(key, getter=lambda item: self.to_kv(item)[Defs.key_ind])
		self._key, self._value = self.to_kv(self.itr.value)
		self.label.set_text(str(self._key))
		if signal:
			self._emit('postchange', self._value)
	key = property(lambda self: self._key, set_key)
	def set_value(self, value, signal=False):
		if signal:
			self._emit('change', self._value)
		value = self.itr.set_value(value, getter=lambda item: self.to_kv(item)[Defs.value_ind])
		self._key, self._value = self.to_kv(self.itr.value)
		self.label.set_text(str(self._key))
		if signal:
			self._emit('postchange', self._value)
	value = property(lambda self: self._value, set_value)
	def decr(self, *args):
		self._emit('change', self._value)
		self._key, self._value = self.to_kv(self.itr.prev(error=False))
		self.label.set_text(str(self._key))
		self._emit('postchange', self._value)
	def incr(self, *args):
		self._emit('change', self._value)
		self._key, self._value = self.to_kv(self.itr.next(error=False))
		self.label.set_text(str(self._key))
		self._emit('postchange', self._value)
	def pack(self, size, focus=False):
		widths = self.column_widths(size, focus)
		columns = sum(widths)
		SingleLogView().log(f'{self.__class__.__name__}.pack()  columns:{columns}  widths:{widths}')
		log and log(f'MinPicker({self.key}).pack: {(widths, )}')
		return columns,
	def max_width(self, eval_end=100):
		eval_end = slice(eval_end)
		result = 0
		for item in self.itr[eval_end]:
			result = max(result, len(str(self.to_kv(item)[Defs.key_ind])))
		return result


class DeltaTimePicker(MinPicker):
	_options = Enum(reversed(list(zip(DeltaTime.names, DeltaTime.denoms))[:-2]))
	
	def __init__(self, **kws):
		super().__init__(itr=self._options, to_kv=self.__class__._as_tuple, **kws)


class MinCheckBox(wid.Columns):
	_sizing = frozenset([wid.FLOW])
	signals = ['change', 'postchange']
	_states_text = ['X', ' ', '#']

	def __init__(self, label=None, state=False, has_mixed=False, fmt='[{}]', states_text=None,
			on_state_change=None, user_data=None, dividechars=0, **kws):
		self.label = SelectableIcon(label or '', 0) if isinstance(label, (str, type(None))) else label
		self.sized_label = self.label if isinstance(self.label, tuple) else ('fixed', len(self.label.text), self.label)
		
		states_text = states_text or self._states_text
		states_text = [fmt.format(text)	for text in states_text]
		icon_len = max(*[len(text) for text in states_text])
		self.states = {state: (FIXED, icon_len, SelectableIcon(text))
			for state, text in zip([True, False, 'mixed'], states_text)}
		
		super().__init__([self.states[state], self.sized_label], dividechars=dividechars, **kws)

		self.has_mixed = has_mixed
		self._state = None
		self.set_state(state)

		if on_state_change:
			connect_signal(self, 'change', on_state_change, user_data)
	def pack(self, size, focus=False):
		widths = self.column_widths(size, focus)
		columns = sum(widths)

		rows = self.rows(size, focus)
		log and log(f'MinCheckBox({self.checked}).pack: {(widths, rows)}')
		return columns, rows
	@staticmethod
	def coerce_item(item):
		is_box_column = False
		size_type, size, widget = range(3)
		item = (item[widget], (GIVEN, item[size], is_box_column))
		return item
	def set_state(self, state, do_callback=True):
		if self._state == state:
			return

		if state not in self.states:
			raise CheckBoxError("%s Invalid state: %s" % (
				repr(self), repr(state)))

		# self._state is None is a special case when the CheckBox
		# has just been created
		old_state = self._state
		if do_callback and old_state is not None:
			self._emit('change', state)
		self._state = state
		# rebuild the display widget with the new state
		# self._w = Columns( [('fixed', self.reserve_columns, self.states[state] ), self.label ] )
		self._contents[0] = self.coerce_item(self.states[state])
		self.focus_col = 0
		if do_callback and old_state is not None:
			self._emit('postchange', state)
	def get_state(self):
		"""Return the state of the checkbox."""
		return self._state
	checked = state = property(get_state, set_state)
	def keypress(self, size, key):
		if self._command_map[key] != ACTIVATE:
			return key

		self.toggle_state()
	def toggle_state(self):
		if self.state == False:
			self.set_state(True)
		elif self.state == True:
			if self.has_mixed:
				self.set_state('mixed')
			else:
				self.set_state(False)
		elif self.state == 'mixed':
			self.set_state(False)
	def mouse_event(self, size, event, button, x, y, focus):
		if button != 1 or not is_mouse_press(event):
			return False
		self.toggle_state()
		return True


class Accordian(wid.Pile):
	def __init__(self, contents, *args, **kws):
		contents = [item for panel in contents for item in [('pack', urwid.Divider('-')), panel]]
		super().__init__(contents, *args, **kws)

		
class DequeWalker(wid.SimpleListWalker):
	def __init__(self, maxlen=100, contents=None):
		self.maxlen = maxlen
		super().__init__(contents or [])
	def __iadd__(self, other):
		super().__iadd__(other)
		self.drop()
		return self
	def dropped(self, item): pass
	def drop(self):
		while self.maxlen < len(self):
			self.dropped(self.pop(0))


class LogView(DequeWalker):
	def __init__(self, maxlen=100, messages=None, hidden=None):
		super().__init__(maxlen)
		self.category_count = Counter()
		self.hidden = hidden or set()
		self.logs(messages or [])
	def count_tags(self):
		pass
	def set_hidden(self, hide=None, unhide=None):
		self.hidden = (self.hidden + set(hide or [])) - set(unhide or [])
	def logs(self, msgs, default_category=None):
		if not isinstance(msgs, list):
			msgs = [msgs]
		items = []
		for item in msgs:
			msg, category = isinstance(item, str) and (item, default_category) or item
			self.category_count[category] += 1
			item = urwid.Text(msg)
			item.category = category
			items.append(item)
		return super().__iadd__(items)
	def log(self, msg, category=None):
		item = urwid.Text(msg)
		item.tags = category
		self.logs([ (msg, category) ])
	__iadd__ = logs
	def dropped(self, item):
		self.category_count[item.category] += 1


class SingleLogView(LogView, IdFactory):
	def __init__(self, *args, id=None, **kws):
		if hasattr(self, 'id'): return
		
		LogView.__init__(self, *args, **kws)
		IdFactory.__init__(self, id)
