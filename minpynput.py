from pynput.mouse import Button as Mouse
import string

from pynput.keyboard import Key,KeyCode
from pyutils.extend.datastructures import XDict


class EventStruct(object):
	''' container defining core member variables and operations required by all derivative events '''
	def __init__(self, event_id=0, quals=0, char=None, name=None, **kws):
		self.event_id = event_id
		self.char = char
		self.quals = quals
		self.name = name

	def __str__(self):
		id = self.name or self.char or self.event_id    # use first non-null
		id = id == self.char and ("'%s'" % id) or id    # wrap in quotes if char KeyEvent
		# quals = self.quals and ', quals=' + EventQualifiers.str(self.quals) or ''  # hide quals when its 0
		result = '%s(%s)' % (self.__class__.__name__, id)
		return result

	def __hash__(self):
		result = (self.event_id or 0) + self.quals << 16
		return result
	#
	# def __eq__(self, other):
	# 	if isinstance(other, str) and len(other) == 1:
	# 		other = KeyEvent(other)
	# 	if isinstance(other, EventStruct):
	# 		result = hash(self) == hash(other)
	# 		return result
	#
	# 	if isinstance(other, Key):
	# 		other = other.value
	# 	if not isinstance(other, KeyCode):
	# 		result = self.event_id == other.value.vk
	# 		result &= self.quals & Eq.key_quals[other]
	# 	else:
	# 		raise TypeError()
	#
	# 	return result
	#
	# def __and__(self, other):
	# 	if isinstance(other, EventStruct):
	# 		raise NotImplementedError()  # results in EventSeq() or succession of user inputs
	# 	elif isinstance(other, int):
	# 		result = copy(self)
	# 		result.quals &= other #| ((not other & (Eq.is_down|Eq.is_up)) and Eq.is_up or 0)  # default to key up if up/down not specified
	# 	else:
	# 		raise TypeError()
	#
	# 	return result
	#
	# def __iand__(self, other):
	# 	if isinstance(other, EventStruct):
	# 		raise NotImplementedError()  # results in EventSeq() or succession of user inputs
	# 	elif isinstance(other, int):
	# 		self.quals &= other #| ((not other & (Eq.is_down|Eq.is_up)) and Eq.is_up or 0)
	# 	else:
	# 		raise TypeError()
	#
	# 	return self
	#
	# def __or__(self, other):
	# 	if isinstance(other, EventStruct):
	# 		raise NotImplementedError()  # results in EventSet() or applicable set of user inputs
	# 	elif isinstance(other, int):
	# 		result = copy(self)
	# 		result.quals |= other #| ((not other & (Eq.is_down|Eq.is_up)) and Eq.is_up or 0)  # default to key up if up/down not specified
	# 	else:
	# 		raise TypeError()
	#
	# 	return result
	#
	# def __ior__(self, other):
	# 	if isinstance(other, EventStruct):
	# 		raise NotImplementedError()  # results in EventSet() or applicable set of user inputs
	# 	elif isinstance(other, int):
	# 		self.quals |= other #| ((not other & (Eq.is_down|Eq.is_up)) and Eq.is_up or 0)
	# 	else:
	# 		raise NotImplementedError()
	#
	# 	return self
	#
	# # method aliases
	# __radd__ = __add__ = __ror__ = __or__
	# __repr__ = __str__
	# verify_unambiguous = EventQualifiers.verify_unambiguous


class Event(EventStruct):
	''' Generic Unified event container for keyboard, mouse, ui or other signals.
		Corresponds to a single occurrence InterfaceSignal within an interface.
	'''
	def __init__(self, signal=None, quals=0, name=None, mouse_pos=None, scroll_pos=None, ui_id=None, device_name=None, **kws):
		# initialize members
		self.ui_id = ui_id
		self.mouse_pos = mouse_pos
		self.scroll_pos = scroll_pos
		self.device_name = device_name

		# coerce some event classes to an operable type
		if isinstance(signal, Key):
			# quals |= Eq.is_key
			name = name or signal.name
			signal = signal.value
		
		# insert appropriate args into base EventStruct constructor
		if isinstance(signal, EventStruct):
			union_kws = XDict(signal.__dict__) | dict(quals=quals|signal.quals)
			EventStruct.__init__(self, **union_kws)
		elif isinstance(signal, str) and len(signal) == 1:
			EventStruct.__init__(self, event_id=ord(signal), quals=quals, char=signal, name=name)
		elif isinstance(signal, KeyCode):
			EventStruct.__init__(self, event_id=signal.vk, quals=quals, char=signal.char, name=name)
		elif isinstance(signal, Mouse):
			EventStruct.__init__(self, event_id=signal.value, quals=quals|Eq.is_mouse, name=name or signal.name)
		# elif isinstance(signal, UISignal):
		# 	raise NotImplementedError()
		else:
			raise TypeError()

		# # resolve qualifiers; coerce
		# self.quals |= self.quals&Eq.any_is_mods			and Eq.is_mod or 0
		# self.quals |= self.quals&Eq.any_is_mod_supers	and Eq.is_mod_super or 0
		# self.quals |= self.quals&Eq.any_has_mods		and Eq.has_mod or 0
		# self.quals |= self.quals&Eq.any_has_mod_supers	and Eq.has_mod_super or 0
		# self.quals |= self.char							and Eq.has_char or 0
		# self.quals |= ((self.char or 0) and not self.quals&Eq.any_has_mod_supers) and Eq.is_char or 0  # is_char requires no mod_supers
		# # 	self.quals |= (isinstance(signal, Key) and Eq.is_key or Eq.is_mouse) | Eq.key_quals.get(signal, 0)
		# # 	self.quals |= (not self.quals & (Eq.is_down|Eq.is_up)) and Eq.is_up or 0  # for keys trigger on release by default
	

class EventSignal:
	''' Utility namespace containing predefined event constants and methods
		to statically access other arbitrary events. Not to be instantiated.
	'''

	# mouse event enumeration
	mouse_left		= Event(Mouse.left)
	mouse_middle	= Event(Mouse.middle)
	mouse_right		= Event(Mouse.right)

	# key event enumeration
	alt				= Event(Key.alt)
	alt_l			= Event(Key.alt_l)
	alt_r			= Event(Key.alt_r)
	alt_gr			= Event(Key.alt_gr)
	backspace		= Event(Key.backspace)
	caps_lock		= Event(Key.caps_lock)
	cmd				= Event(Key.cmd)
	cmd_l			= Event(Key.cmd_l)
	cmd_r			= Event(Key.cmd_r)
	ctrl			= Event(Key.ctrl)
	ctrl_l			= Event(Key.ctrl_l)
	ctrl_r			= Event(Key.ctrl_r)
	delete			= Event(Key.delete)
	down			= Event(Key.down)
	end				= Event(Key.end)
	enter			= Event(Key.enter)
	escape			= Event(Key.esc)
	f1				= Event(Key.f1)
	f2				= Event(Key.f2)
	f3				= Event(Key.f3)
	f4				= Event(Key.f4)
	f5				= Event(Key.f5)
	f6				= Event(Key.f6)
	f7				= Event(Key.f7)
	f8				= Event(Key.f8)
	f9				= Event(Key.f9)
	f10				= Event(Key.f10)
	f11				= Event(Key.f11)
	f12				= Event(Key.f12)
	f13				= Event(Key.f13)
	f14				= Event(Key.f14)
	f15				= Event(Key.f15)
	f16				= Event(Key.f16)
	f17				= Event(Key.f17)
	f18				= Event(Key.f18)
	f19				= Event(Key.f19)
	f20				= Event(Key.f20)
	home			= Event(Key.home)
	left			= Event(Key.left)
	page_down		= Event(Key.page_down)
	page_up			= Event(Key.page_up)
	right			= Event(Key.right)
	shift			= Event(Key.shift)
	shift_l			= Event(Key.shift_l)
	shift_r			= Event(Key.shift_r)
	space			= Event(Key.space)
	tab				= Event(Key.tab)
	up				= Event(Key.up)
	insert			= Event(Key.insert)
	menu			= Event(Key.menu)
	num_lock		= Event(Key.num_lock)
	pause			= Event(Key.pause)
	print_screen 	= Event(Key.print_screen)
	scroll_lock		= Event(Key.scroll_lock)
	
	# shorthand event aliases
	rms				= mouse_left
	mms				= mouse_middle
	lms				= mouse_right
	esc				= escape
	ins				= insert
	ret				= enter
	del_			= delete
	bksp			= backspace
	pgdn			= page_down
	pgup			= page_up
	prtscr 			= print_screen
	scrlk			= scroll_lock
	caplk			= caps_lock
	numlk			= num_lock

	@staticmethod
	def setup():
		
		for char in '_'+string.ascii_lowercase:
			setattr(EventSignal, char, Event(char))

Es = EventSignal()
EventSignal.setup()

