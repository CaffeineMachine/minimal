import argparse
from pprint import pprint as pp

import itertools


import signal
import inspect
import os
import re
import sys
import traceback
from time import sleep
from subprocess import Popen, PIPE, STDOUT
from pyutils.extend.datastructures import *

try:
    from subprocess import DEVNULL # py3k
except ImportError:
    import os
    DEVNULL = open(os.devnull, 'wb')

ARG_MISSING = '<ARG_MISSING>'
cl_word_split_regex = re.compile(r'"([^"]*)"|' + "'([^']*)'|(\S+)")
# result = [item  for group in result  for item in group  if item]
cl_args_split_regex = re.compile(r'--\S+ ("([^"])*"|' + "'([^'])*'|\S*)|\S+")
cl_kwarg_split_regex = re.compile(r'(--\S+) (.+)')







# def message(a, b):
# 	print('Manual termination')
# 	sys.exit(0)
#
# original_sigint_handler = signal.getsignal(signal.SIGINT)
# signal.signal( signal.SIGINT, signal.SIG_IGN )
# # signal.signal( signal.SIGKILL, signal.SIG_IGN )
# # print('started')
# # proc = Popen(['sh', '-c', 'for i in `seq 1 9`; do echo .; sleep $i; done'])
# signal.signal(signal.SIGINT, message)
# # # signal.signal( signal.SIGINT, original_sigint_handler)
# # print('stalling 5')
# # sleep(5)
# # print('stalling 5')
# # sleep(5)
# # print('stalling 5')
# # sleep(5)
# # print('timeout')
# # sys.exit(0)



def exec_func_args(*args, **kwargs):
	''' '''
	command = cl_from_func_args(*args, **kwargs)
	command_str = ' '.join(command)

	# temporarily disable interrupt signal to configure child handling via inheritance
	signal.signal(signal.SIGINT, signal.SIG_IGN)
	# create child process
	proc = Popen(command, stdout=DEVNULL, stderr=STDOUT)
	# set parent process interrupt sig handler to exit with success exit code to not disrupt child processes
	signal.signal(signal.SIGINT, lambda _, __: sys.exit(0))

	return proc

def clstr_from_func_args(*args, **kwargs):
	return ' '.join(cl_from_func_args(*args, **kwargs))

def cl_from_func_args(*args, **kwargs):
	result = list(args) + [item  for kw, arg in kwargs.items()  for item in ('--'+str(kw), '"%s"'%arg)]
	return result

def result_from_cl_args(callable, args=None):
	''' '''
	func_args, func_kwargs  = func_from_cl_args(callable, args)
	result = callable(*func_args, **func_kwargs)
	return result

def func_from_cl_args(callable, args=None):
	''' '''
	sys_argv = sys.argv

	try:
		# remove any args the python interpreter is responsible for before/including the python file being executed
		stacktrace_py_files = set([stack_level[0] for stack_level in traceback.extract_stack()])
		for index, sys_arg in enumerate(sys_argv):
			if os.path.abspath(sys_arg) in stacktrace_py_files:
				sys_argv = sys_argv[index+1:]
				break

	except:	pass

	callable = isinstance(callable, type) and callable.__init__ or callable
	argspec = inspect.getargspec(callable)
	if argspec.args and 'self' in argspec.args: argspec.args.remove('self')
	func_args, func_defaults = list(argspec.args or []), list(argspec.defaults or [])
	kwdefaults = {}
	while func_defaults:
		kw = func_args.pop()
		default = func_defaults.pop()
		kwdefaults[kw] = default

	cli_args, cli_kwargs = [], kwdefaults
	while len(sys_argv):
		arg = sys_argv.pop(0)
		flag = arg.split('--', 1)
		if len(flag) == 2:
			arg = sys_argv.pop(0)
			cli_kwargs[ flag[1] ] = arg
		else:
			cli_args += [arg]

	return cli_args, cli_kwargs


# def parse_construct_(callable, args=None):
# 	''' '''
# 	sys_argv = sys.argv
#
# 	try:
# 		# remove any args the python interpreter is responsible for before/including the python file being executed
# 		stacktrace_py_files = set([stack_level[0] for stack_level in traceback.extract_stack()])
# 		for index, sys_arg in enumerate(sys_argv):
# 			if os.path.abspath(sys_arg) in stacktrace_py_files:
# 				sys_argv = sys_argv[index+1:]
#
# 	except:	pass
#
#
# 	# dynamically generate argparser corresponding to function parameters
# 	callable = isinstance(callable, type) and callable.__init__ or callable
# 	parser = argparse.ArgumentParser(prog=callable.__name__)
# 	argspec = inspect.getargspec(callable)
# 	temp_args = argspec.args
# 	temp_args.remove('self')
# 	while temp_args:
# 		required = len(temp_args) <= len(argspec.defaults)
# 		arg = temp_args.pop(0)
# 		parser.add_argument((required and '--' or '') + arg, default=ARG_MISSING)
#
# 	# interpret cli parameters using argparse and remove missing arguments from kwargs
# 	parser.print_help()
# 	result = parser.parse_args(sys_argv)
# 	result = {kw:arg for kw, arg in result._get_kwargs() if arg != ARG_MISSING}
# 	return result

'''
class CommandParser:
	def __init__(self, pre_run=None, post_run=None, *, default=None, order=None, **commands):
		self.pre_run = pre_run
		self.post_run = post_run
		self.default = default or []
		self.order = order
		self.commands = commands
		self.parser = self.config()
		
		# sanity checks
		err_str = (f'{self.__class__.__name__}: {{which}} command(s) {{miss}} '
				  f'not found in available commands: {list(commands)}.')
		if default and not set(default).issubset(set(commands)):
			raise ValueError(err_str.format(which='Default', miss=set(default) - set(commands)))
		# if order and not set(order).issubset(set(commands)):
		# 	raise ValueError(err_str.format(which='Order', miss=set(order) - set(commands)))
	def config(self):
		parser = argparse.ArgumentParser()
		for name, command in self.commands.items():
			if callable(command):
				parser.add_argument(f'-{name}', action='store_true')	# call <name>(*args) when present
			else:
				parser.add_argument(f'-{name}')							# set <name> when present
			# parser.add_argument(f'-{name}')
		return parser
	def run(self, cl_args=None):
		cmd_args, cmd_kws = (), {}
		# run user defined setup logic
		if self.pre_run:
			result = self.pre_run(*cmd_args, **cmd_kws)
			if result and isinstance(result, tuple):
				cmd_args, cmd_kws = result
			
		# coerced cl_args to a list or None
		if isinstance(cl_args, str):
			cl_args = Strs(cl_args)
		found_cl = vars(self.parser.parse_args(cl_args))
		
		# use defaults when parser result is empty
		if not found_cl:
			found_cl = dict(zip(self.default, [True]*len(self.default)))
			
		# found_cl = {name:value for name, value in found_cl.items() if value is not False}
		# found_cl_cmds = [(name, self.commands.get(name, value)) for name, value in found_cl.items() if value is not False]
		
		# found_cl_cmds = []
		# for name, value in found_cl.items():
		# 	if value is not False: continue
		# 	if value is True:
		# 		value = self.commands[name]  # cl_arg is present
		# 	found_cl_cmds.append((name, value))
		
		# # reorder when order is specified
		# if self.order:
		# 	found_cl_cmds = dict(found_cl_cmds)
		# 	found_cl_cmds = [(name, found_cl_cmds[name]) for name in self.order if name in found_cl_cmds]
		
		# execute commands corresponding to command line args
		for name in self.order:
			if name not in found_cl or found_cl[name] is False: continue
			
			if found_cl[name] is not True:
				cmd_kws[name] = found_cl[name]  # add command line value for name to cmd_kws
			else:
				command = self.commands[name]
				result = command(*cmd_args, **cmd_kws)
				if result and isinstance(result, tuple):
					cmd_args, cmd_kws = result
		
		# run user defined shutdown logic
		if self.post_run:
			self.post_run(*cmd_args, **cmd_kws)
'''
# class CustomAction(argparse.Action):
# 	def __init__(self, action, name=None, **kws):
# 		self.action = action
# 		name = name or self.action.__name__
# 		super().__init__(['-'+name], name, type=type(self.action), **kws)
# 	# def __call__(self, parser, namespace, values, option_string=None):
# 	def __call__(self, *args, **kws):
# 		print('CustomAction.__call__')
# 		self.action(*args, **kws)
# 		return self

from pyutils.utilities.introspection import *
from collections import OrderedDict as odict
class Undefined:
	def __eq__(self, other):
		result = isinstance(other, Undefined) or other in [Undefined, 'Undefined', 'undefined']
		return result

class CommandLineOptions:
	''' Translates attributes within instance of self into an ArgParser
		Locates all patterns in command line args like like ... -attr_name [params]...
		- 'self.<attr_name> = params' is performed in execution when attr is a variable or property
		- 'self.<attr_name>(*args)' is performed in execution when attr is a method
	'''
	_exec_on_ctor = False
	def __init__(self, pre_run=None, post_run=None, *, default=None, order=None, **options):
		self._pre_run = pre_run
		self._post_run = post_run
		self._default = default or []
		self._order = order
		self._type_names = {}
		self._options = dict(self._options_from_instance(), **options)
		self._parser = self._gen_parser(self._options)
		
		# sanity checks
		err_str = (f'{self.__class__.__name__}: {{which}} command(s) {{miss}} '
				  f'not found in available commands: {list(options)}.')
		if default and not set(default).issubset(set(options)):
			raise ValueError(err_str.format(which='Default', miss=set(default) - set(options)))
		# if order and not set(order).issubset(set(commands)):
		# 	raise ValueError(err_str.format(which='Order', miss=set(order) - set(commands)))
		
		if self._exec_on_ctor:
			self._execute()
	def _options_from_instance(self, inst=None):
		inst = inst if inst is not None else self
		# result = inst.__dict__
		result = XDict.unlike(
			dict(
				inst.__dict__.items(),
				# **inst._options,
				**{name: getattr(inst, name) for name in vars(inst.__class__)}),
			'^_')
		return result
	def _gen_parser(self, options=None):
		parser = argparse.ArgumentParser()
		options = options or self._options
		for name, command in options.items():
			self._type_names.setdefault(callable(command) and 'callable' or 'config', []).append(name)
			parser.add_argument(f'--{name}', dest=name, action='append', nargs='*')
			
		return parser
	def _execute(self, cl_args=None):
		cmd_args, cmd_kws = (), {}
		# run user defined setup logic
		if self._pre_run:
			result = self._pre_run(*cmd_args, **cmd_kws)
			if result and isinstance(result, tuple):
				cmd_args, cmd_kws = result
				
				
				
				
		# separate given options into functions and config fields
		funcs, configs = {}, {}
		for name, value in self._options.items():
			target = funcs if callable(value) else configs
			target[name] = value
			
		methods_ordered = odict(method_order(self))
		methods_ordered_names = XList.unlike(methods_ordered, '^_')
		funcs_ordered = odict(XDict.arrangment(funcs, self._order or [...]+methods_ordered_names))
		
		
				
			
		# coerced cl_args to a list or None
		if isinstance(cl_args, str):
			cl_args = Strs(cl_args)
		parsed = vars(self._parser.parse_args(cl_args))
		parsed_kws = {}
		for name, value in list(parsed.items()):
			if value is None: continue
			parsed_kws[name] = value[0] if name in funcs_ordered else value[0][0]
		configs = XDict.union(configs, parsed_kws)
		funcs_args, configs  = XDict.subsets(configs, funcs)
		# funcs_args, configs  = XDict.subsets(configs, parsed_kws)

		
		if not parsed_kws:
			self._parser.print_help()
			return
		
		# execute commands corresponding to command line args
		for name, option in funcs_ordered.items():
			if option is None or funcs_args.get(name) is None: continue  # funcs_args[name] is None when omitted
			
			cmd_args = []
			result = option(*(funcs_args[name] or []), **configs)
			if result and isinstance(result, tuple):
				cmd_args, cmd_kws = result

		
class CommandLineAction(CommandLineOptions):
	''' Same functionality as CommandLineOptions with the exception that the ctor also
		performs command line operations
	'''
	_exec_on_ctor = True

from dataclasses import dataclass

class TestCL(CommandLineOptions):
	def __init__(self):
		super().__init__(foo=0)
	def bar(self, *args, **kws):
		print(f'bar(*{args}, **{kws})')
	def baz(self, *args, **kws):
		print(f'baz(*{args}, **{kws})')
def test_detect():

	test_cl = TestCL()
	# test_cl._gen_parser().parse_args(Strs('--help'))
	parser = test_cl._gen_parser()
	args = Strs('-foo 1 2 3 -bar -foo a b c')
	pp(vars(parser.parse_args(args)))
	
def test_execute():
	test_cl = TestCL()
	args = Strs('-bar 1 2 3 -foo a')
	test_cl._execute(args)


if __name__=='__main__':
	test_detect()
	test_execute()
