from pandas import *
from .minnumpy import truth_of_coll

#region minpandas reassignment
Series.__bool__ = Series.__nonzero__			= truth_of_coll
DataFrame.__bool__ = DataFrame.__nonzero__		= truth_of_coll
Index.__bool__ = Index.__nonzero__				= truth_of_coll
#endregion

#region inline test
if __name__=='__main__':
	from time import sleep
	
	# initialization
	xdf = DataFrame([0.]*2, columns=['field'])
	first_if_true = True and xdf or {}
	second_if_false = False and xdf or {}
	
	# results
	print(xdf)              # => [ 0.  0.  0.  0.  0.]
	print(bool(xdf))        # => True
	print(first_if_true)    # => [ 0.  0.  0.  0.  0.]
	print(second_if_false)  # => []
	sleep(.1)
	
	# the following results in the exception:
	# ValueError: The truth value of an array with more than one element is ambiguous. Use a.any() or a.all()
	try: error = True and DataFrame([0.]*5, columns=['field']) or {}
	except ValueError as e: print(f'Normal result bool(DataFrame(...)):\n  "{e!r}"')
#endregion
