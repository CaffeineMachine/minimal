from datetime import datetime as T

t_now = T.now()
t_last = globals().get('t_last',t_now)
td = int((t_now-t_last).total_seconds()+.4999999)
fmt_t = '%y/%m/%d %H:%M:%S'
print('t_last: {:{f}}\nt_now:  {:{f}}\ntd:     {:>+17d} secs\n'.format(t_now, t_last, td, f=fmt_t))
t_last = t_now