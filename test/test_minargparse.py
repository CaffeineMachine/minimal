import sys
import minimal.minargparse
from pprint import pprint as pp
from pyutils.extend.datastructures import *
from subprocess import Popen


class Obj(object):
	def __init__(self, a, b, c=3, d=3, e=4):
		''' constructor '''
		self.__dict__.update(XDict(locals()) ^ ['self'])

def test_parse_construct():
	''' exercise basic operations '''

	return minargparse.func_from_cl_args(Obj)

# def test_():
# 	''' exercise basic operations '''
# 	print sys.argv
# 	pp(traceback.extract_stack())

if __name__ == '__main__':
	print __file__
	sys.exit()
	sys.argv = ['python', './test_minargparse.py', '1', '2']
	print(test_parse_construct())
	sys.argv = ['./test_minargparse.py', '1', '2']
	print(test_parse_construct())
	sys.argv = ['--test', '2', '--try', '3']
	print(test_parse_construct())
	sys.argv = ['1']
	print(test_parse_construct())
