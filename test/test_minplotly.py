from minimal.minplotly import minGraph
from time import sleep

def test_graph_stream():
	''' minimal required operation entails creating a minGraph and incrementally adding data '''
	graph = minGraph('test-series-stream-a')
	graph.streaming_timeout_secs = 4
	for data_point in list(enumerate(range(2,-1,-1))):
		graph += data_point
		sleep(2)

	# session cleanup
	sleep(8)								# idle for timeout to trigger close_on_idle()
	graph.close_on_idle(force_close=True)	# any subsequent call should be a noop

	# todo: test reopening session with same instance
	# for data_point in list(enumerate(range(9,-1,-1))):
	# 	graph += data_point
	# 	sleep(2)

if __name__ in ('__main__', 'test_minplotly'):
	test_graph_stream()
